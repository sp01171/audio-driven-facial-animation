#from __future__ import print_function, division
#import pandas as pd
#from skimage import io, transform
import os
import pickle
import torch
import numpy as np
from torch.utils.data import Dataset, DataLoader
#import matplotlib.pyplot as plt
#from torchvision import transforms, utils
#from PIL import Image
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

# Ignore warnings
#import warnings
#warnings.filterwarnings("ignore")

#plt.ion()   # interactive mode


class FaceLandmarkMfccDataset(Dataset):
    """Face Landmarks dataset."""


    # Note - ONLY CURRENT SETUP TO LOAD CLIP1 DATASET, NEED TO ADD A FOR-LOOP FOR OTHERS
    def __init__(self):
        """
        Arguments:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """

        # WOULD NEED TO ENCAPSULATE THE CODE BELOW INTO A FOR-LOOP TO COVER EVERY VIDEO CLIP

        self.cumulative_landmark_array = []
        self.cumulative_confidence_score_array = []
        self.cumulative_mfcc_array = []
        #clip_num = 1 # Includes the entire dataset
        clip_num = 2 # Excludes vid1 from dataset
        
        while (clip_num <= 20):
            print()
            print(clip_num)
            landmark_directory_path = "C:/GitHub/audio-driven-facial-animation/data - Copy/clip" + str(clip_num) + "/landmarks/"
            mfcc_directory_path = "C:/GitHub/audio-driven-facial-animation/data - Copy/clip" + str(clip_num) + "/audio/"

            """Load landmark frame arrays and mfcc lines"""

            # Creates a 2D array containing all landmarks, with each row being an array containing frame landmarks from a different video clip (1-20), with each of those arrays containing 68 landmarks
            single_clip_landmarks_and_confidence_scores, landmark_filecount = self.load_landmarks_and_confidence_scores(landmark_directory_path)
            print("landmark_filecount =", landmark_filecount)

            # Creates a 2D array containing all MFCCs, with each row being an array containing frame MFCCs from a different video clip (1-20), with each of those arrays containing 13 MFCCs
            single_clip_mfccs = self.load_mfccs(mfcc_directory_path, landmark_filecount)

            #frame_num = 0
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0]) # 68 landmarks in an array, with dtype value
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0][0]) # 68 landmarks without dtype or outer array
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0][0][0]) # singular [x, y] landmark coord
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0][1]) # dtype value
            #print(single_clip_landmarks_and_confidence_scores[frame_num][1]) # 68 landmark confidence scores in an array
            frame_num = 0
            while (frame_num < landmark_filecount):
                #print("frame_num =", frame_num)
                #print(single_clip_landmarks_and_confidence_scores[frame_num][0][0]) # 68 landmarks without dtype or outer array
                #print(single_clip_landmarks_and_confidence_scores[frame_num][1][0]) # 68 landmark confidence scores in an array
                self.cumulative_landmark_array.append(single_clip_landmarks_and_confidence_scores[frame_num][0][0])
                self.cumulative_confidence_score_array.append(single_clip_landmarks_and_confidence_scores[frame_num][1][0])
                self.cumulative_mfcc_array.append(single_clip_mfccs[frame_num])
                frame_num += 1
            #print(self.cumulative_landmark_array[3])
            #print(self.cumulative_confidence_score_array[3])
            #print(type(self.cumulative_landmark_array))
            #print(type(self.cumulative_confidence_score_array))

            """Load mfcc lines <DEPRECATED CODE BELOW>"""
            #single_clip_mfccs = self.load_mfccs(mfcc_directory_path, landmark_filecount)
            #self.cumulative_mfcc_array.append(single_clip_mfccs)
            #print(self.cumulative_mfcc_array)
            #print(type(self.cumulative_mfcc_array))

            clip_num += 1

        ##########################################

        self.cumulative_landmark_array = np.array(self.cumulative_landmark_array)
        self.cumulative_confidence_score_array = np.array(self.cumulative_confidence_score_array)
        #print(type(self.cumulative_landmark_array))
        #print(type(self.cumulative_confidence_score_array))
        #print(self.cumulative_landmark_array[3])
        #print(self.cumulative_confidence_score_array[3])
        print("\ncumulative_landmark_array =", self.cumulative_landmark_array.dtype)
        print("cumulative_confidence_score_array =", self.cumulative_confidence_score_array.dtype)
        self.cumulative_landmark_array = torch.from_numpy(self.cumulative_landmark_array)
        self.cumulative_confidence_score_array = torch.from_numpy(self.cumulative_confidence_score_array)
        #self.cumulative_landmark_array = torch.tensor(self.cumulative_landmark_array) # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        #self.cumulative_confidence_score_array = torch.tensor(self.cumulative_confidence_score_array) # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        #print(type(self.cumulative_landmark_array))
        #print(type(self.cumulative_confidence_score_array))
        #print(self.cumulative_landmark_array[3])
        #print(self.cumulative_confidence_score_array[3])
        print("cumulative_landmark_array =", self.cumulative_landmark_array.dtype)
        print("cumulative_confidence_score_array =", self.cumulative_confidence_score_array.dtype)

        #print(type(self.cumulative_mfcc_array))
        #print(self.cumulative_mfcc_array)
        self.cumulative_mfcc_array = np.array(self.cumulative_mfcc_array, dtype='float32')
        #print(type(self.cumulative_mfcc_array))
        #print(self.cumulative_mfcc_array)
        print("cumulative_mfcc_array =", self.cumulative_mfcc_array.dtype)
        self.cumulative_mfcc_array = torch.from_numpy(self.cumulative_mfcc_array)
        #self.cumulative_mfcc_array = torch.tensor(self.cumulative_mfcc_array) # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        #print(type(self.cumulative_mfcc_array))
        #print(self.cumulative_mfcc_array)
        #print(self.cumulative_mfcc_array[146])
        print("cumulative_mfcc_array =", self.cumulative_mfcc_array.dtype)

        """# Size confirmation testing for dataset including vid1-vid20
        print("\n", type(self.cumulative_landmark_array))
        print("self.cumulative_landmark_array[2742] =\n", self.cumulative_landmark_array[2742])
        print("\n", type(self.cumulative_confidence_score_array))
        print("self.cumulative_confidence_score_array[2742] =\n", self.cumulative_confidence_score_array[2742])
        print("\n", type(self.cumulative_mfcc_array))
        print("self.cumulative_mfcc_array[2742] =\n", self.cumulative_mfcc_array[2742], "\n")"""

        # Size confirmation testing for dataset including vid2-vid20
        """print("\n", type(self.cumulative_landmark_array))
        print("self.cumulative_landmark_array[2595] =\n", self.cumulative_landmark_array[2595])
        print("\n", type(self.cumulative_confidence_score_array))
        print("self.cumulative_confidence_score_array[2595] =\n", self.cumulative_confidence_score_array[2595])
        print("\n", type(self.cumulative_mfcc_array))
        print("self.cumulative_mfcc_array[2595] =\n", self.cumulative_mfcc_array[2595], "\n")"""

        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_landmark_array_vid2.txt", self.cumulative_landmark_array[0], fmt='%0.1f')
        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_confidence_score_array.txt", self.cumulative_confidence_score_array, fmt='%f')
        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_mfcc_array.txt", self.cumulative_mfcc_array, fmt='%f')

        # We move our tensor to the GPU if available
        if torch.cuda.is_available():
            self.cumulative_landmark_array = self.cumulative_landmark_array.to('cuda')
            self.cumulative_confidence_score_array = self.cumulative_confidence_score_array.to('cuda')
            self.cumulative_mfcc_array = self.cumulative_mfcc_array.to('cuda')
            print(f"Device tensors are stored on: {self.cumulative_mfcc_array.device}")
    
    def load_landmarks_and_confidence_scores(self, directory_in_str):
        landmark_and_confidence_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".p"):
                #print(filename) 
                #print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                #print(filepath)
                # LOAD PICKLE FILE & APPEND INTO ARRAY
                # With-statement causes file to close as soon as the indented code below is executed
                with open(filepath, "rb") as f:
                    landmark_and_confidence_array.append(pickle.load(f))
                #print(landmark_array)
                # EXTRACT COORDS LATER?
                counter += 1
                #print("counter =", counter)
            else:
                continue
        #print(landmark_array)
        #print("counter =", counter)
        return landmark_and_confidence_array, counter
    

    def load_mfccs(self, directory_in_str, local_landmark_filecount):
        mfcc_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".npy") and counter < local_landmark_filecount:
                #print(filename) 
                #print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                #print(filepath)
                # LOAD .npy file & APPEND INTO ARRAY
                # NEED TO CONSIDER WHEN & HOW TO CONVERT INTO TENSOR FORMAT, AND IF LOADING THE .npy FILES WILL LOAD THEM AS NUMPY ARRAYS IN THE FIRST PLACE!
                #mfcc_array.append(pickle.load(open(filepath, "rb")))
                mfcc_array.append(np.load(filepath))
                #print("<LOAD MFCC HERE>")
                #print(mfcc_array)
                # EXTRACT COORDS LATER?
                counter += 1
            elif counter < local_landmark_filecount:
                print("Non npy file found.")
                continue
            else:
                print("Frame count matched, breaking loop...")
                break
        print("counter =", counter)
        print("local_landmark_filecount =", local_landmark_filecount)
        return mfcc_array
    

    def __len__(self):
        #return len(face_dataset.cumulative_mfcc_array[0][:])
        return len(face_dataset.cumulative_mfcc_array)


    def __getitem__(self, idx):
        # If idx is a tensor
        if torch.is_tensor(idx):
            idx = idx.tolist()

        #mfccs

        #landmarks = landmarks.astype('float').reshape(-1, 2)
        
        sample = {'mfccs': face_dataset.cumulative_mfcc_array[idx], 'landmarks': face_dataset.cumulative_landmark_array[idx], 'confidence_scores': face_dataset.cumulative_confidence_score_array[idx]}

        # SHOULD I BE PARSING THE ORIGINAL DATA OR A COPY OF THE DATA?

        """if self.transform:
            sample = self.transform(sample)"""

        return sample


face_dataset = FaceLandmarkMfccDataset()
#print(face_dataset.cumulative_landmark_array)

#print(face_dataset.cumulative_landmark_array[0][3][0][0]) #Note that here the 2nd index is for which frame we should obtain the landmarks for
# Maybe it would be for the best to reformat the array into two separate ones, one with just the coords and one with just the certainty index...

#print(face_dataset.cumulative_mfcc_array)
#print(face_dataset.cumulative_mfcc_array[0][1]) #Note that here the 2nd index is for which frame we should obtain the mfccs for

#np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/mfcc_full_import.txt", face_dataset.cumulative_mfcc_array[0][:])

#print("__len__() return =", face_dataset.__len__())
print("__len__() return =", len(face_dataset), "\n")

"""for i in range(len(face_dataset)):
    sample = face_dataset[i]
    print(i, sample['mfccs'].shape, sample['landmarks'].shape, sample['confidence_scores'].shape)
    #print(i, sample['mfccs'], sample['landmarks'])"""

trainloader = DataLoader(face_dataset, batch_size=4, shuffle=True, num_workers=0)

if __name__ == '__main__':
    for i_batch, sample_batched in enumerate(trainloader):
        print(i_batch, sample_batched['landmarks'].size(), sample_batched['confidence_scores'].size(), sample_batched['mfccs'].size())

        # observe 4th batch and stop.
        """if i_batch == 3:
            break"""

print()

"""dataloader = DataLoader(face_dataset, batch_size=4, shuffle=False, num_workers=0)

# Helper function to show a batch - I don't think this works, had to scuff together my own "batch" of images, but the landmarks don't seem to match...
def show_landmarks_batch(frame_num, sample_batched):
    vid_num = 1
    img_tensor_list = [] # List of tensors
    while (frame_num < 4):
        print(frame_num)
        new_frame_path = ('C:/GitHub/audio-driven-facial-animation/data/clip' + str(vid_num+1) + '/images/image' + str(frame_num).zfill(4) + '.jpg')
        img = Image.open(new_frame_path)
        print(type(img))
        print("img =\n", img)
        transform = transforms.Compose([
            transforms.PILToTensor()
        ])
        img_tensor = transform(img)
        print("img_tensor =\n", img_tensor)
        print(type(img_tensor))
        img_tensor_list.append(img_tensor)
        frame_num += 1
    img_batch = torch.stack(img_tensor_list) # Converts the list of tensors into a larger tensor
    print("img_batch type = \n", type(img_batch))

    # Show image with landmarks for a batch of samples.
        # .cpu() copies the landmark tensors back onto system memory, so it can be used in the code below
    images_batch, landmarks_batch = img_batch, sample_batched['landmarks'].cpu() 
    batch_size = len(images_batch)
    im_size = images_batch.size(2)
    grid_border_size = 2

    print("i_batch val =", i_batch)
    grid = utils.make_grid(images_batch)
    plt.imshow(grid.numpy().transpose((1, 2, 0)))

    for i in range(batch_size):
        plt.scatter(landmarks_batch[i, :, 0].numpy() + i * im_size + (i + 1) * grid_border_size,
                    landmarks_batch[i, :, 1].numpy() + grid_border_size,
                    s=10, marker='.', c='r')

        plt.title('Batch from dataloader')

if __name__ == '__main__':
    for i_batch, sample_batched in enumerate(dataloader):
        #print(i_batch, sample_batched['landmarks'].size(), sample_batched['confidence_scores'].size(), sample_batched['mfccs'].size())

        # observe 4th batch and stop.
        if i_batch == 3:
            plt.figure()
            print(i_batch, sample_batched['landmarks'].size(), sample_batched['confidence_scores'].size(), sample_batched['mfccs'].size())
            show_landmarks_batch(i_batch-3, sample_batched)
            plt.axis('off')
            plt.ioff()
            plt.show()
            break"""

# [4, 13] -> [4, 68, 2], where batch size is 4
# [13 x 1] -> [(68 x 2) x 1]
# Fully Connected Layer

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        #self.conv1 = nn.Conv2d(13, 68, 5)
        #self.pool = nn.MaxPool2d(2, 2)
        #self.fc1 = nn.Linear(13 * 1, 68 * 2)
        self.fc1 = nn.Linear(13 * 1, 136 * 1)

    def forward(self, x):
        #x = self.pool(F.relu(self.conv1(x)))
        #print("Before torch.flatten(x, 1), x.shape =", x.shape)
        #x = torch.flatten(x, 1) # flatten all dimensions except batch
        #print("After torch.flatten(x, 1), x.shape =", x.shape)
        #print("x =", x.dtype)
        output = self.fc1(x)
        #print("output =", output.dtype)
        return output

net = Net().to('cuda')
if torch.cuda.is_available():
    net = net.to('cuda')

#criterion = nn.CrossEntropyLoss()
#criterion = nn.MSELoss() # Squared L2 Norm

# can just use euclidean distance from prediction to ground truth coords? L1 or L2
# need to flatten to 136x1 vector instead of 68x2
# check that dataloader is working properly
# train NN on just 1 sample
# train NN on maybe like 12 samples
# add batches and complete NN for audio to landmarks using all samples
# create simple visualisation tool for co-ords (predictions vs ground truth) to be able to debug what's going on
# record all this stuff in report


criterion = nn.L1Loss() # L1 Norm

optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9) # Stochastic Gradient Descent



for epoch in range(120):  # loop over the dataset multiple times, started to stabilize around 120ish?

    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        #mfccs, landmarks, confidence_scores = data
        input = data['mfccs']
        #print("input =\n", input)
        target = data['landmarks']

        #print("Before torch.flatten(target, 1), targer.shape =", target.shape)
        target = torch.flatten(target, 1) # flatten all dimensions except batch
        #print("After torch.flatten(target, 1), target.shape =", target.shape)

        #print("target =\n", target)
        #print("input =", type(input))
        #print("target =", type(landmarks))
        #print("input =", input.dtype)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        #print("input.shape =", input.shape)
        #print("Before .view(), input.shape =", input.shape)
        #input = input.view(1, -1)
        #print("After .view(), input.shape =", input.shape)
        #print("Before unsqueeze(input, 2), input.shape =", input.shape)
        #input = torch.unsqueeze(input, 2) # IS THIS WHAT'S NEEDED?
        #print("After unsqueeze(input, 2), input.shape =", input.shape)
        #print("Before .view(), target.shape =", target.shape)
        output = net(input)
        #target = target.view(1, -1)
        #target = target.view(-1, 1)
        #print("After .view(), target.shape =", target.shape)
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.item()
        if i % 400 == 399:    # print every 400 mini-batches
            print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss / 400:.3f}')
            running_loss = 0.0

print('Finished Training')

print("script complete!")
