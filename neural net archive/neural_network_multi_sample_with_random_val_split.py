#from __future__ import print_function, division
#import pandas as pd
#from skimage import io, transform
import os
import pickle
import torch
import numpy as np
from torch.utils.data import Dataset, DataLoader#, random_split
from torch.utils.data.sampler import SubsetRandomSampler
import matplotlib.pyplot as plt
from torchvision import transforms, utils
#from PIL import Image
import torch.nn as nn
#import torch.nn.functional as F
import torch.optim as optim
from math import ceil

import pprint

from torchvision.io import read_image
#from torchvision.utils import draw_keypoints
#import torchvision.transforms.functional as F

# Ignore warnings
#import warnings
#warnings.filterwarnings("ignore")

plt.ion()   # interactive mode

NUM_OF_EPOCHS = 20
#SIZE_OF_BATCHES = 1
SIZE_OF_BATCHES = 24

START_CLIP = 2
END_CLIP = 19 # Leaves 20 as test data

VALIDATION_TRAINING_SPLIT = 0.2
SHUFFLE_VAL_TRAIN_SPLIT = True

TEST_SET_TOGGLE = 0
TEST_START_CLIP = 20
TEST_END_CLIP = 20

VISUAL_FRAME_NUM = 0
LANDMARK_SELECTION = 0


class FaceLandmarkMfccDataset(Dataset):
    """Face Landmarks dataset."""


    # Note - ONLY CURRENT SETUP TO LOAD CLIP1 DATASET, NEED TO ADD A FOR-LOOP FOR OTHERS
    def __init__(self):
        """
        Arguments:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """

        # WOULD NEED TO ENCAPSULATE THE CODE BELOW INTO A FOR-LOOP TO COVER EVERY VIDEO CLIP

        self.cumulative_landmark_array = []
        self.cumulative_confidence_score_array = []
        self.cumulative_mfcc_array = []

        if (TEST_SET_TOGGLE == 0):
            clip_num = START_CLIP
            upper_clip_bound = END_CLIP
        elif (TEST_SET_TOGGLE == 1):
            clip_num = TEST_START_CLIP
            upper_clip_bound = TEST_END_CLIP
        else:
            print("Error - Upper and lower bounds for dataset are invalid!")
            exit()
        
        while (clip_num <= upper_clip_bound):
            print()
            print(clip_num)
            landmark_directory_path = "C:/GitHub/audio-driven-facial-animation/data/clip" + str(clip_num) + "/landmarks/"
            mfcc_directory_path = "C:/GitHub/audio-driven-facial-animation/data/clip" + str(clip_num) + "/audio/"

            """Load landmark frame arrays and mfcc lines"""

            # Creates a 2D array containing all landmarks, with each row being an array containing frame landmarks from a different video clip (1-20), with each of those arrays containing 68 landmarks
            single_clip_landmarks_and_confidence_scores, landmark_filecount = self.load_landmarks_and_confidence_scores(landmark_directory_path)
            print("landmark_filecount =", landmark_filecount)

            # Creates a 2D array containing all MFCCs, with each row being an array containing frame MFCCs from a different video clip (1-20), with each of those arrays containing 13 MFCCs
            single_clip_mfccs = self.load_mfccs(mfcc_directory_path, landmark_filecount)

            #frame_num = 0
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0]) # 68 landmarks in an array, with dtype value
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0][0]) # 68 landmarks without dtype or outer array
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0][0][0]) # singular [x, y] landmark coord
            #print(single_clip_landmarks_and_confidence_scores[frame_num][0][1]) # dtype value
            #print(single_clip_landmarks_and_confidence_scores[frame_num][1]) # 68 landmark confidence scores in an array
            frame_num = 0
            #while (frame_num < NUM_OF_SAMPLES):
            while (frame_num < landmark_filecount):
                #print("frame_num =", frame_num)
                #print(single_clip_landmarks_and_confidence_scores[frame_num][0][0]) # 68 landmarks without dtype or outer array
                #print(single_clip_landmarks_and_confidence_scores[frame_num][1][0]) # 68 landmark confidence scores in an array
                self.cumulative_landmark_array.append(single_clip_landmarks_and_confidence_scores[frame_num][0][0])
                self.cumulative_confidence_score_array.append(single_clip_landmarks_and_confidence_scores[frame_num][1][0])
                self.cumulative_mfcc_array.append(single_clip_mfccs[frame_num])
                frame_num += 1
            #print(self.cumulative_landmark_array[3])
            #print(self.cumulative_confidence_score_array[3])
            #print(type(self.cumulative_landmark_array))
            #print(type(self.cumulative_confidence_score_array))

            """Load mfcc lines <DEPRECATED CODE BELOW>"""
            #single_clip_mfccs = self.load_mfccs(mfcc_directory_path, landmark_filecount)
            #self.cumulative_mfcc_array.append(single_clip_mfccs)
            #print(self.cumulative_mfcc_array)
            #print(type(self.cumulative_mfcc_array))

            clip_num += 1

        ##########################################

        self.cumulative_landmark_array = np.array(self.cumulative_landmark_array)
        self.cumulative_confidence_score_array = np.array(self.cumulative_confidence_score_array)
        #print(type(self.cumulative_landmark_array))
        #print(type(self.cumulative_confidence_score_array))
        #print(self.cumulative_landmark_array[3])
        #print(self.cumulative_confidence_score_array[3])
        print("\ncumulative_landmark_array =", self.cumulative_landmark_array.dtype)
        print("cumulative_confidence_score_array =", self.cumulative_confidence_score_array.dtype)
        self.cumulative_landmark_array = torch.from_numpy(self.cumulative_landmark_array)
        self.cumulative_confidence_score_array = torch.from_numpy(self.cumulative_confidence_score_array)
        #self.cumulative_landmark_array = torch.tensor(self.cumulative_landmark_array) # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        #self.cumulative_confidence_score_array = torch.tensor(self.cumulative_confidence_score_array) # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        #print(type(self.cumulative_landmark_array))
        #print(type(self.cumulative_confidence_score_array))
        #print(self.cumulative_landmark_array[3])
        #print(self.cumulative_confidence_score_array[3])
        print("cumulative_landmark_array =", self.cumulative_landmark_array.dtype)
        print("cumulative_confidence_score_array =", self.cumulative_confidence_score_array.dtype)

        #print(type(self.cumulative_mfcc_array))
        #print(self.cumulative_mfcc_array)
        self.cumulative_mfcc_array = np.array(self.cumulative_mfcc_array, dtype='float32')
        #print(type(self.cumulative_mfcc_array))
        #print(self.cumulative_mfcc_array)
        print("cumulative_mfcc_array =", self.cumulative_mfcc_array.dtype)
        self.cumulative_mfcc_array = torch.from_numpy(self.cumulative_mfcc_array)
        #self.cumulative_mfcc_array = torch.tensor(self.cumulative_mfcc_array) # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        #print(type(self.cumulative_mfcc_array))
        #print(self.cumulative_mfcc_array)
        #print(self.cumulative_mfcc_array[146])
        print("cumulative_mfcc_array =", self.cumulative_mfcc_array.dtype)

        """# Size confirmation testing for dataset including vid1-vid20
        print("\n", type(self.cumulative_landmark_array))
        print("self.cumulative_landmark_array[2742] =\n", self.cumulative_landmark_array[2742])
        print("\n", type(self.cumulative_confidence_score_array))
        print("self.cumulative_confidence_score_array[2742] =\n", self.cumulative_confidence_score_array[2742])
        print("\n", type(self.cumulative_mfcc_array))
        print("self.cumulative_mfcc_array[2742] =\n", self.cumulative_mfcc_array[2742], "\n")"""

        # Size confirmation testing for dataset including vid2-vid20
        """print("\n", type(self.cumulative_landmark_array))
        print("self.cumulative_landmark_array[2595] =\n", self.cumulative_landmark_array[2595])
        print("\n", type(self.cumulative_confidence_score_array))
        print("self.cumulative_confidence_score_array[2595] =\n", self.cumulative_confidence_score_array[2595])
        print("\n", type(self.cumulative_mfcc_array))
        print("self.cumulative_mfcc_array[2595] =\n", self.cumulative_mfcc_array[2595], "\n")"""

        print("Number of samples = ", len(self.cumulative_landmark_array))

        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_landmark_array_vid2.txt", self.cumulative_landmark_array[0], fmt='%0.1f')
        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_confidence_score_array.txt", self.cumulative_confidence_score_array, fmt='%f')
        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_mfcc_array.txt", self.cumulative_mfcc_array, fmt='%f')

        # We move our tensor to the GPU if available
        if torch.cuda.is_available():
            self.cumulative_landmark_array = self.cumulative_landmark_array.to('cuda')
            self.cumulative_confidence_score_array = self.cumulative_confidence_score_array.to('cuda')
            self.cumulative_mfcc_array = self.cumulative_mfcc_array.to('cuda')
            print(f"Device tensors are stored on: {self.cumulative_mfcc_array.device}")
    
    def load_landmarks_and_confidence_scores(self, directory_in_str):
        landmark_and_confidence_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".p"):
                #print(filename) 
                #print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                #print(filepath)
                # LOAD PICKLE FILE & APPEND INTO ARRAY
                # With-statement causes file to close as soon as the indented code below is executed
                with open(filepath, "rb") as f:
                    landmark_and_confidence_array.append(pickle.load(f))
                #print(landmark_array)
                # EXTRACT COORDS LATER?
                counter += 1
                #print("counter =", counter)
            else:
                continue
        #print(landmark_array)
        #print("counter =", counter)
        return landmark_and_confidence_array, counter
    

    def load_mfccs(self, directory_in_str, local_landmark_filecount):
        mfcc_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".npy") and counter < local_landmark_filecount:
                #print(filename) 
                #print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                #print(filepath)
                # LOAD .npy file & APPEND INTO ARRAY
                # NEED TO CONSIDER WHEN & HOW TO CONVERT INTO TENSOR FORMAT, AND IF LOADING THE .npy FILES WILL LOAD THEM AS NUMPY ARRAYS IN THE FIRST PLACE!
                #mfcc_array.append(pickle.load(open(filepath, "rb")))
                mfcc_array.append(np.load(filepath))
                #print("<LOAD MFCC HERE>")
                #print(mfcc_array)
                # EXTRACT COORDS LATER?
                counter += 1
            elif counter < local_landmark_filecount:
                #print("Non npy file found.")
                continue
            else:
                print("Frame count matched, breaking loop...")
                break
        print("counter =", counter)
        print("local_landmark_filecount =", local_landmark_filecount)
        return mfcc_array
    

    def __len__(self):
        #return len(self.cumulative_mfcc_array[0][:])
        return len(self.cumulative_mfcc_array)


    def __getitem__(self, idx):
        # If idx is a tensor
        if torch.is_tensor(idx):
            idx = idx.tolist()

        #mfccs

        #landmarks = landmarks.astype('float').reshape(-1, 2)
        
        sample = {'mfccs': self.cumulative_mfcc_array[idx], 'landmarks': self.cumulative_landmark_array[idx], 'confidence_scores': self.cumulative_confidence_score_array[idx]}

        # SHOULD I BE PARSING THE ORIGINAL DATA OR A COPY OF THE DATA?

        """if self.transform:
            sample = self.transform(sample)"""

        return sample


training_and_validation_dataset = FaceLandmarkMfccDataset()
print("Training & Validation Data loaded successfully!")

#print(training_and_validation_dataset.cumulative_landmark_array)

#print(training_and_validation_dataset.cumulative_landmark_array[0][3][0][0]) #Note that here the 2nd index is for which frame we should obtain the landmarks for
# Maybe it would be for the best to reformat the array into two separate ones, one with just the coords and one with just the certainty index...

#print(training_and_validation_dataset.cumulative_mfcc_array)
#print(training_and_validation_dataset.cumulative_mfcc_array[0][1]) #Note that here the 2nd index is for which frame we should obtain the mfccs for

#np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/mfcc_full_import.txt", training_and_validation_dataset.cumulative_mfcc_array[0][:])

#print("__len__() return =", training_and_validation_dataset.__len__())
print("__len__() return =", len(training_and_validation_dataset), "\n")

"""for i in range(len(training_and_validation_dataset)):
    sample = training_and_validation_dataset[i]
    print(i, sample['mfccs'].shape, sample['landmarks'].shape, sample['confidence_scores'].shape)
    #print(i, sample['mfccs'], sample['landmarks'])"""

"""train_size = int(TRAINING_VALIDATION_SPLIT * len(training_and_validation_dataset))
val_size = len(training_and_validation_dataset) - train_size
training_dataset, validation_dataset = random_split(training_and_validation_dataset, [train_size, val_size])
print("Training & Validation Datasets split successfully at " + str(int(TRAINING_VALIDATION_SPLIT * 100)) + ":" + str(int(100 - (100 * TRAINING_VALIDATION_SPLIT))) + " !")
print("__len__() return =", len(training_dataset))
print("__len__() return =", len(validation_dataset))

TEST_SET_TOGGLE = 1
test_dataset = FaceLandmarkMfccDataset()
print("Testing Dataset loaded successfully!")
print("__len__() return =", len(test_dataset), "\n")

trainloader = DataLoader(training_dataset, batch_size=SIZE_OF_BATCHES, shuffle=False, num_workers=0)
print("Trainloader created successfully!")
valloader = DataLoader(validation_dataset, batch_size=SIZE_OF_BATCHES, shuffle=False, num_workers=0)
print("Valloader created successfully!")
testloader = DataLoader(test_dataset, batch_size=SIZE_OF_BATCHES, shuffle=False, num_workers=0)
print("Testloader created successfully!\n")"""
# The block of commented code above successfully splits the training and validation datasets,
# but doesn't retain index information about the datasets,so it's then impossible to know which images make up which dataset split...



TEST_SET_TOGGLE = 1
test_dataset = FaceLandmarkMfccDataset()
print("Testing Dataset loaded successfully!")
print("__len__() return =", len(test_dataset), "\n")

#validation_split = .2
validation_split = VALIDATION_TRAINING_SPLIT
print("validation_split =", validation_split)
shuffle_dataset = SHUFFLE_VAL_TRAIN_SPLIT
#random_seed= 42

# Creating data indices for training and validation splits:
dataset_size = len(training_and_validation_dataset)
print("dataset_size =", dataset_size)

indices = list(range(dataset_size))
split = int(np.floor(validation_split * dataset_size))
train_split = int(dataset_size - split)
print("split =", split)
print("train_split =", train_split)
if shuffle_dataset :
    #np.random.seed(random_seed)
    np.random.shuffle(indices)
train_indices, val_indices = indices[split:], indices[:split]
print("train_indices =", train_indices)
print("val_indices =", val_indices)

# Creating PT data samplers and loaders:
train_sampler = SubsetRandomSampler(train_indices)
valid_sampler = SubsetRandomSampler(val_indices)

trainloader = DataLoader(training_and_validation_dataset, batch_size=SIZE_OF_BATCHES, shuffle=False, sampler=train_sampler, num_workers=0)
print("Trainloader created successfully!")
valloader = DataLoader(training_and_validation_dataset, batch_size=SIZE_OF_BATCHES, shuffle=False, sampler=valid_sampler, num_workers=0)
print("Valloader created successfully!")

#testloader = DataLoader(test_dataset, batch_size=SIZE_OF_BATCHES, shuffle=False, num_workers=0)
testloader = DataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=0)
print("Testloader created successfully!\n")










"""if __name__ == '__main__':
    for i_batch, sample_batched in enumerate(trainloader):
        print(i_batch, sample_batched['landmarks'].size(), sample_batched['confidence_scores'].size(), sample_batched['mfccs'].size())

        # observe 4th batch and stop.
        #if i_batch == 3:
            #break
print()"""


# [4, 13] -> [4, 68, 2], where batch size is 4
# [13 x 1] -> [(68 x 2) x 1]
# Fully Connected Layer

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        #self.conv1 = nn.Conv2d(13, 68, 5)
        #self.pool = nn.MaxPool2d(2, 2)
        #self.fc1 = nn.Linear(13 * 1, 68 * 2)
        self.fc1 = nn.Linear(13 * 1, 136 * 1)

    def forward(self, x):
        #x = self.pool(F.relu(self.conv1(x)))
        #print("Before torch.flatten(x, 1), x.shape =", x.shape)
        #x = torch.flatten(x, 1) # flatten all dimensions except batch
        #print("After torch.flatten(x, 1), x.shape =", x.shape)
        #print("x =", x.dtype)
        output = self.fc1(x)
        #print("output =", output.dtype)
        return output


net = Net().to('cuda')
if torch.cuda.is_available():
    net = net.to('cuda')
    print("Net() successfully loaded onto CUDA!")

#criterion = nn.CrossEntropyLoss()
#criterion = nn.MSELoss() # Squared L2 Norm

# can just use euclidean distance from prediction to ground truth coords? L1 or L2
# need to flatten to 136x1 vector instead of 68x2
# check that dataloader is working properly
# train NN on just 1 sample
# train NN on maybe like 12 samples
# add batches and complete NN for audio to landmarks using all samples
# create simple visualisation tool for co-ords (predictions vs ground truth) to be able to debug what's going on
# record all this stuff in report

criterion = nn.L1Loss() # L1 Norm

optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9) # Stochastic Gradient Descent

epoch_losses = []
val_epoch_losses = []
print("len(trainloader) =", len(trainloader))
print("len(valloader) =", len(valloader))
for epoch in range(NUM_OF_EPOCHS):  # loop over the dataset multiple times, started to stabilize around 120ish? #600 epochs show partial accuracy!
    cumulative_loss = 0
    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        #mfccs, landmarks, confidence_scores = data
        input = data['mfccs']
        #print("input =\n", input)
        target = data['landmarks']

        #print("Before torch.flatten(target, 1), targer.shape =", target.shape)
        target = torch.flatten(target, 1) # flatten all dimensions except batch
        #print("After torch.flatten(target, 1), target.shape =", target.shape)

        #print("target =\n", target)
        #print("input =", type(input))
        #print("target =", type(landmarks))
        #print("input =", input.dtype)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        #print("input.shape =", input.shape)
        #print("Before .view(), input.shape =", input.shape)
        #input = input.view(1, -1)
        #print("After .view(), input.shape =", input.shape)
        #print("Before unsqueeze(input, 2), input.shape =", input.shape)
        #input = torch.unsqueeze(input, 2) # IS THIS WHAT'S NEEDED?
        #print("After unsqueeze(input, 2), input.shape =", input.shape)
        #print("Before .view(), target.shape =", target.shape)
        output = net(input)
        #target = target.view(1, -1)
        #target = target.view(-1, 1)
        #print("After .view(), target.shape =", target.shape)
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.item()
        """if i % 10 == 9:    # print every 100 mini-batches
            print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss / 10:.3f}')
            running_loss = 0.0"""
        epoch_size = len(trainloader)
        if i % epoch_size == (epoch_size-1):    # print every epoch
            print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss:.3f}')
            #running_loss = 0.0 # WHY IS THIS HERE????
        #print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss / 1:.3f}')
        cumulative_loss += running_loss
        running_loss = 0.0
    # Without dividing the batch size, it gives us the batch for each batch, when dividing is gives us the loss for each epoch(?)
    #epoch_losses.append(cumulative_loss / SIZE_OF_BATCHES)
    #print("len() of trainloader =", len(trainloader))
    epoch_losses.append(cumulative_loss / len(trainloader))
    # Calculate losses for validation dataset after each training epoch
    cumulative_val_loss = 0
    running_val_loss = 0.0
    for i, data in enumerate(valloader, 0):
        input = data['mfccs']
        target = data['landmarks']
        target = torch.flatten(target, 1) # flatten all dimensions except batch
        output = net(input)
        loss = criterion(output, target)
        running_val_loss += loss.item()
        cumulative_val_loss += running_val_loss
        running_val_loss = 0.0
    val_epoch_losses.append(cumulative_val_loss / len(valloader))

#print("\nval_epoch_losses =\n", val_epoch_losses, "\n")

print('Finished Training')

"""PATH = './ADFA_net.pth'
torch.save(net.state_dict(), PATH)
print('Neural Network Saved')"""


def predict_test_image():
    # Now tests on the first frame of the test set
    img_tensor_list = [] # List of tensors
    """while (frame_num < 1):
        print("frame_num =", frame_num)
        new_frame_path = ('C:/GitHub/audio-driven-facial-animation/data/clip' + str(TEST_START_CLIP) + '/images/image' + str(VISUAL_FRAME_NUMBER).zfill(4) + '.jpg')
        img_tensor = read_image(new_frame_path)
        #print("img_tensor =\n", img_tensor)
        #print(type(img_tensor))
        img_tensor_list.append(img_tensor)
        frame_num += 1"""
    while (0 < 1):
        new_frame_path = ('C:/GitHub/audio-driven-facial-animation/data/clip' + str(TEST_START_CLIP) + '/images/image' + str(VISUAL_FRAME_NUM).zfill(4) + '.jpg')
        print("new_frame_path =", new_frame_path)
        img_tensor = read_image(new_frame_path)
        #print("img_tensor =\n", img_tensor)
        #print(type(img_tensor))
        img_tensor_list.append(img_tensor)
        break
    img_batch = torch.stack(img_tensor_list) # Converts the list of tensors into a larger tensor
    #print("img_batch type = \n", type(img_batch))

    dataiter = iter(testloader)
    #print(next(dataiter))
    #mfccs, landmarks, confidence_scores = next(dataiter)
    sample = next(dataiter)
    mfccs = sample['mfccs']
    landmarks = sample['landmarks']
    #print("\nmfccs =\n", mfccs)
    print("\nlandmarks =\n", landmarks)
    output_tensor = net(mfccs)
    print("\noutput_tensor =\n", output_tensor)

    """print("\noutput_tensor[0] =\n", output_tensor[0])
    print("\noutput_tensor[0][0] =\n", output_tensor[0][0])
    print("\noutput_tensor[0][0].item() =\n", output_tensor[0][0].item())"""
    #print("\noutput_tensor.size() =\n", output_tensor.size())

    return img_batch, landmarks, output_tensor


def calculate_mean_test_loss():
    cumulative_test_loss = 0
    running_test_loss = 0.0
    mean_test_frame_loss_arr = []
    print("len(testloader) =", len(testloader))
    for i, data in enumerate(testloader, 0):
        input = data['mfccs']
        target = data['landmarks']
        target = torch.flatten(target, 1) # flatten all dimensions except batch
        output = net(input)
        loss = criterion(output, target)
        running_test_loss += loss.item()
        #print(i)
        #print("running_test_loss =", running_test_loss)
        cumulative_test_loss += running_test_loss
        mean_test_frame_loss_arr.append(running_test_loss)
        running_test_loss = 0.0
    mean_test_loss = cumulative_test_loss / len(testloader)
    return mean_test_loss, mean_test_frame_loss_arr


def plot_mean_test_frame_errors(mean_test_frame_loss_arr): # Plots mean loss for each frame in the test set

    print()
    print(len(mean_test_frame_loss_arr))
    print(max(mean_test_frame_loss_arr))
    print(ceil(max(mean_test_frame_loss_arr)/10)*10)

    """fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    frames = range(136)
    ax.bar(frames, mean_test_frame_loss_arr)"""

    plt.figure()
    frames = range(136)
    plt.bar(frames, mean_test_frame_loss_arr, color ='maroon', width = 0.4)

    """plt.plot(np.array(epoch_losses_arr), color='r', label = "Training")
    plt.plot(np.array(val_epoch_losses_arr), color='g', label = "Validation")
    plt.xlim(0, len(epoch_losses_arr))
    plt.ylim(0, (ceil(max(epoch_losses_arr)/10)*10))"""
    
    plt.xlim(0, len(mean_test_frame_loss_arr))
    plt.ylim(0, (ceil(max(mean_test_frame_loss_arr)/10)*10))

    #ax.set_title("Mean error for all frames in testing dataset")
    #ax.set_xlabel("Frames")
    #ax.set_ylabel("Mean loss (L1 Norm)")
    plt.title("Mean error for all frames in testing dataset")
    plt.xlabel("Frames")
    plt.ylabel("Mean loss (L1 Norm)")
    #plt.ioff()
    #plt.show()


def plot_individual_landmark_deviations(individual_landmark_loss_arr_list): # Plots means and standard deviations of the 68 facial landmarks, across all 136 test frames
    
    # Divide the arr_list into 68 lists for each landmark? Maybe unneccessary...
    # Create a list of the means of each landmark
    # Create a list of the standard deviations of each landmark

    mean_landmark_loss_arr = []
    ordered_landmark_loss_arr = []
    landmark_loss_std_arr = []
    i = 0
    while (i < len(individual_landmark_loss_arr_list[0])): # Should iterate 68 times
        landmark_loss_arr = []
        for arr in individual_landmark_loss_arr_list: # Appends all landmarks for position # into an array
            landmark_loss_arr.append(arr[i])
        """if (i == 0): # First loop
            print("\n", individual_landmark_loss_arr_list[0][0])
            print(individual_landmark_loss_arr_list[1][0])
            print("landmark_loss_total = \n", landmark_loss_total, "\n")"""
        # ordered_landmark_loss_arr contains all losses ordered by landmark, in sub arrays - VERY IMPORTANT INFORMATION
        ordered_landmark_loss_arr.append(landmark_loss_arr)
        # Calculate standard deviation
        landmark_loss_std = np.std(np.array(landmark_loss_arr))
        landmark_loss_std_arr.append(landmark_loss_std)
        # Calculate mean average
        mean_landmark_loss = sum(landmark_loss_arr) / len(landmark_loss_arr)
        mean_landmark_loss_arr.append(mean_landmark_loss)
        i += 1
    print("mean_landmark_loss_arr = \n", mean_landmark_loss_arr, "\n") # PLOT THIS ARRAY!
    print("len(landmark_loss_std_arr) = \n", len(landmark_loss_std_arr), "\n")
    print("landmark_loss_std_arr = \n", landmark_loss_std_arr, "\n")

    plt.figure()
    plt.errorbar(range(68), mean_landmark_loss_arr, landmark_loss_std_arr, linestyle='None', capsize=3, fmt='o') #, marker='^')
    plt.title("Mean landmark loss for test set")
    plt.xlabel("Landmark Number")
    plt.ylabel("Mean loss and standard deviation (L1 Norm)")

    return ordered_landmark_loss_arr # Returns the 2D array so that it can be used in eval plot 3! ordered_landmark_loss_arr[landmark number 0-67][frame number 0-135] -> loss value

"""pred_types = {'face': pred_type(slice(0, 17), (0.682, 0.780, 0.909, 0.5)),
              'eyebrow1': pred_type(slice(17, 22), (1.0, 0.498, 0.055, 0.4)),
              'eyebrow2': pred_type(slice(22, 27), (1.0, 0.498, 0.055, 0.4)),
              'nose': pred_type(slice(27, 31), (0.345, 0.239, 0.443, 0.4)),
              'nostril': pred_type(slice(31, 36), (0.345, 0.239, 0.443, 0.4)),
              'eye1': pred_type(slice(36, 42), (0.596, 0.875, 0.541, 0.3)),
              'eye2': pred_type(slice(42, 48), (0.596, 0.875, 0.541, 0.3)),
              'lips': pred_type(slice(48, 60), (0.596, 0.875, 0.541, 0.3)),
              'teeth': pred_type(slice(60, 68), (0.596, 0.875, 0.541, 0.4))
              }"""


def plot_single_landmark_losses(ordered_landmark_loss_arr, landmark_selection): # Plot loss for a single landmark over all frames
    
    plt.figure()
    frames = range(136)
    plt.bar(frames, ordered_landmark_loss_arr[landmark_selection], color ='olive', width = 0.4)

    """plt.plot(np.array(epoch_losses_arr), color='r', label = "Training")
    plt.plot(np.array(val_epoch_losses_arr), color='g', label = "Validation")
    plt.xlim(0, len(epoch_losses_arr))
    plt.ylim(0, (ceil(max(epoch_losses_arr)/10)*10))"""
    
    plt.xlim(0, len(ordered_landmark_loss_arr[landmark_selection]))
    plt.ylim(0, (ceil(max(ordered_landmark_loss_arr[landmark_selection])/10)*10))

    #ax.set_title("Mean error for all frames in testing dataset")
    #ax.set_xlabel("Frames")
    #ax.set_ylabel("Mean loss (L1 Norm)")
    plt.title("Landmark " + str(landmark_selection) + ": Loss for all frames in testing dataset")
    plt.xlabel("Frames")
    plt.ylabel("Loss (L1 Norm)")



def calculate_individual_landmark_test_losses():
    criterion_individual = nn.L1Loss(reduction='none')
    criterion_mean = nn.L1Loss(reduction='mean')
    individual_landmark_loss_arr_list = []
    #print("len(testloader) =", len(testloader))
    for i, data in enumerate(testloader, 0):
        input = data['mfccs']
        target = data['landmarks']
        target = torch.flatten(target, 1) # flatten all dimensions except batch
        output = net(input)
        loss_individual = criterion_individual(output, target)
        loss_mean = criterion_mean(output, target)
        #print("\nloss_individual.size() = ", loss_individual.size(), "\n")
        #print("loss_individual =\n", loss_individual, "\n")
        #print("loss_mean =\n", loss_mean, "\n")

        individual_loss_arr = []
        j = 0
        while j < 136:
            if (j % 2) == 0: #checks if even
                #organised_output.append([round(output_tensor[0][i].item(), 1), round(output_tensor[0][i+1].item(), 1)])
                #individual_loss_arr.append(loss_individual[0][j].item() + loss_individual[0][j+1].item())
                individual_loss_arr.append((loss_individual[0][j].item() + loss_individual[0][j+1].item()) / 2) # CHECK THISSSSSSSSSS
            j += 2
            #print(j)
        individual_landmark_loss_arr_list.append(individual_loss_arr)

        #if (i == 2):
            #break # only test first frame...
    
    #print("\nlen(individual_landmark_loss_arr_list[1]) = ", len(individual_landmark_loss_arr_list[0]))
    #print("\nindividual_landmark_loss_arr_list = \n", individual_landmark_loss_arr_list, "\n")
    
    #print("\norganised_output_list=")
    #pprint.pprint(organised_output)

    return individual_landmark_loss_arr_list


def plot_landmarks(img_batch, landmarks, output_tensor):
    organised_output = []
    organised_output_list = []
    i = 0
    while i < 136:
        if (i % 2) == 0: #checks if even
            #organised_output.append([round(output_tensor[0][i].item(), 1), round(output_tensor[0][i+1].item(), 1)])
            organised_output.append([output_tensor[0][i].item(), output_tensor[0][i+1].item()])
        i += 2
        #print(i)
    organised_output_list.append(organised_output)
    #print("\norganised_output_list=")
    #pprint.pprint(organised_output_list)

    # Show image with landmarks for a batch of samples.
    images_batch, landmarks_batch = img_batch.cpu() , landmarks.cpu() # .cpu() copies the landmark tensors back onto system memory, so it can be used in the code below
    batch_size = len(images_batch)
    print("len(images_batch) =", batch_size)
    im_size = images_batch.size(2) # Don't know why it's size(2)...
    #print("im_size = images_batch.size(2) =", im_size)
    #grid_border_size = 2
    grid_border_size = 0 # We're only displaying one image at a time, so there is no border for subsequent images in the grid!
    #print("i_batch val =", i_batch)
    grid = utils.make_grid(images_batch)
    plt.imshow(grid.numpy().transpose((1, 2, 0)))
    for i in range(batch_size):
        landmark_x_val = landmarks_batch[i, :, 0].numpy() + i * im_size + (i + 1) * grid_border_size
        landmark_y_val = landmarks_batch[i, :, 1].numpy() + grid_border_size
        #print("landmarks_batch[i, :, 0].numpy() =", landmarks_batch[i, :, 0].numpy())
        #print("landmarks_batch[i, :, 1].numpy() =", landmarks_batch[i, :, 1].numpy())
        plt.scatter(landmark_x_val,
                    landmark_y_val,
                    s=70, marker='.', c='r')
        prediction_x_val = np.array(organised_output_list)[i, :, 0] + i * im_size + (i + 1) * grid_border_size
        prediction_y_val = np.array(organised_output_list)[i, :, 1] + grid_border_size
        #print("np.array(organised_output_list)[i, :, 0] =", np.array(organised_output_list)[i, :, 0])
        #print("np.array(organised_output_list)[i, :, 1] =", np.array(organised_output_list)[i, :, 1])
        plt.scatter(prediction_x_val,
                    prediction_y_val,
                    s=10, marker='.', c='b')
        #plt.title("Batch Size: " + str(SIZE_OF_BATCHES) + ", Samples: " + str(NUM_OF_SAMPLES) + ", Epochs: " + str(NUM_OF_EPOCHS))test_NN_predi
        plt.title("Test Image: clip" + str(TEST_START_CLIP) + "/images/image" + str(VISUAL_FRAME_NUM).zfill(4) + ".jpg\n" + "Batch Size: " + str(SIZE_OF_BATCHES) + ", Samples: " + str(train_split) + ", Epochs: " + str(NUM_OF_EPOCHS))
        #plt.figure()
        plt.axis('off')
        #plt.ioff()
        #plt.show()


def plot_loss_against_epochs(epoch_losses_arr, val_epoch_losses_arr, mean_test_loss):
    print("\nepoch_losses =\n", epoch_losses_arr)
    print("\nval_epoch_losses =\n", val_epoch_losses_arr)

    print()
    print(len(epoch_losses_arr))
    print(max(epoch_losses_arr))
    print(ceil(max(epoch_losses_arr)/10)*10)
    print(len(val_epoch_losses_arr))
    print(max(val_epoch_losses_arr))
    print(ceil(max(val_epoch_losses_arr)/10)*10)

    plt.figure()

    plt.plot(np.array(epoch_losses_arr), color='r', label = "Training")
    plt.plot(np.array(val_epoch_losses_arr), color='g', label = "Validation")
    plt.xlim(0, len(epoch_losses_arr))
    plt.ylim(0, (ceil(max(epoch_losses_arr)/10)*10))
    #plt.title("Training Dataset: clip" + str(START_CLIP) + "-" + str(END_CLIP) + ", Final Loss: " + str(round(epoch_losses_arr[-1], 1)) + "\nLoss against Epochs" + " (Batch Size: " + str(SIZE_OF_BATCHES) + ", Epochs: " + str(NUM_OF_EPOCHS) + ")")
    plt.title("Final Training Loss: " + str(round(epoch_losses_arr[-1], 1)) + ", Mean Testing Loss: " + str(round(mean_test_loss, 1)) + "\nLoss against Epochs" + " (Batch Size: " + str(SIZE_OF_BATCHES) + ", Epochs: " + str(NUM_OF_EPOCHS) + ")")
    plt.xlabel("Epochs")
    plt.ylabel("Mean Loss per Epoch (L1 Norm)")
    plt.legend()
    #plt.ioff()
    #plt.show()



img_batch, landmarks, output_tensor = predict_test_image()

mean_test_loss, mean_test_frame_loss_arr = calculate_mean_test_loss()
print("\nmean_test_loss =", mean_test_loss, "\n")
print("\nmean_test_frame_loss_arr =", mean_test_frame_loss_arr, "\n")

individual_landmark_loss_arr_list = calculate_individual_landmark_test_losses()

plot_landmarks(img_batch, landmarks, output_tensor)
plot_loss_against_epochs(epoch_losses, val_epoch_losses, mean_test_loss)
plot_mean_test_frame_errors(mean_test_frame_loss_arr)
ordered_landmark_loss_arr = plot_individual_landmark_deviations(individual_landmark_loss_arr_list)
plot_single_landmark_losses(ordered_landmark_loss_arr, LANDMARK_SELECTION)

plt.ioff()
#plt.legend()
plt.show()

print("script complete!")
