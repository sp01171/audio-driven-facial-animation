#from __future__ import print_function, division
import os
import pickle
import torch
#import pandas as pd
#from skimage import io, transform
import numpy as np
#import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
#from torchvision import transforms, utils

# Ignore warnings
#import warnings
#warnings.filterwarnings("ignore")

#plt.ion()   # interactive mode


class FaceLandmarkMfccDataset(Dataset):
    """Face Landmarks dataset."""


    # Note - ONLY CURRENT SETUP TO LOAD CLIP1 DATASET, NEED TO ADD A FOR-LOOP FOR OTHERS
    def __init__(self):
        """
        Arguments:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """

        # WOULD NEED TO ENCAPSULATE THE CODE BELOW INTO A FOR-LOOP TO COVER EVERY VIDEO CLIP

        """Load landmark frame arrays"""

        # Creates a 2D array containing all landmarks, with each row being an array containing frame landmarks from a different video clip (1-20), with each of those arrays containing 68 landmarks
        self.cumulative_landmark_array = []
        landmark_directory_path = "C:/GitHub/audio-driven-facial-animation/data - Copy/clip1/landmarks/"
        single_clip_landmarks, landmark_filecount = self.load_landmarks(landmark_directory_path)
        print("landmark_filecount =", landmark_filecount)
        self.cumulative_landmark_array.append(single_clip_landmarks)

        """Load mfcc lines"""

        # Creates a 2D array containing all MFCCs, with each row being an array containing frame MFCCs from a different video clip (1-20), with each of those arrays containing 13 MFCCs
        self.cumulative_mfcc_array = []
        landmark_directory_path = "C:/GitHub/audio-driven-facial-animation/data - Copy/clip1/audio/"
        single_clip_mfccs = self.load_mfccs(landmark_directory_path, landmark_filecount)
        self.cumulative_mfcc_array.append(single_clip_mfccs)
    
    
    def load_landmarks(self, directory_in_str):
        landmark_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".p"):
                #print(filename) 
                #print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                print(filepath)
                # LOAD PICKLE FILE & APPEND INTO ARRAY
                # With-statement causes file to close as soon as the indented code below is executed
                with open(filepath, "rb") as f:
                    landmark_array.append(pickle.load(f))
                #print(landmark_array)
                # EXTRACT COORDS LATER?
                counter += 1
                print("counter =", counter)
            else:
                continue
        #print(landmark_array)
        return landmark_array, counter
    

    def load_mfccs(self, directory_in_str, local_landmark_filecount):
        mfcc_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".npy") and counter < local_landmark_filecount:
                #print(filename) 
                #print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                print(filepath)
                # LOAD .npy file & APPEND INTO ARRAY
                # NEED TO CONSIDER WHEN & HOW TO CONVERT INTO TENSOR FORMAT, AND IF LOADING THE .npy FILES WILL LOAD THEM AS NUMPY ARRAYS IN THE FIRST PLACE!
                #mfcc_array.append(pickle.load(open(filepath, "rb")))
                mfcc_array.append(np.load(filepath))
                #print("<LOAD MFCC HERE>")
                #print(mfcc_array)
                # EXTRACT COORDS LATER?
                counter += 1
            elif counter < local_landmark_filecount:
                print("Non npy file found.")
                continue
            else:
                print("Frame count matched, breaking loop...")
                break
        print("counter =", counter)
        print("local_landmark_filecount =", local_landmark_filecount)
        return mfcc_array
    

    def __len__(self):
        return len(face_dataset.cumulative_mfcc_array[0][:])


    def __getitem__(self, idx):
        # If idx is a tensor
        if torch.is_tensor(idx):
            idx = idx.tolist()

        #mfccs

        #landmarks = landmarks.astype('float').reshape(-1, 2)
        sample = {'mfccs': face_dataset.cumulative_mfcc_array[0][idx], 'landmarks': face_dataset.cumulative_landmark_array[0][idx][0][0]}

        # SHOULD I BE PARSING THE ORIGINAL DATA OR A COPY OF THE DATA?

        """if self.transform:
            sample = self.transform(sample)"""

        return sample


face_dataset = FaceLandmarkMfccDataset()
#print(face_dataset.cumulative_landmark_array)

#print(face_dataset.cumulative_landmark_array[0][3][0][0]) #Note that here the 2nd index is for which frame we should obtain the landmarks for
# Maybe it would be for the best to reformat the array into two separate ones, one with just the coords and one with just the certainty index...

#print(face_dataset.cumulative_mfcc_array)
#print(face_dataset.cumulative_mfcc_array[0][1]) #Note that here the 2nd index is for which frame we should obtain the mfccs for

np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/mfcc_full_import.txt", face_dataset.cumulative_mfcc_array[0][:])

#OK SO FOR SOME REASON THE CUMULATIVE MFCC ARRAY IS WITHIN A SUBARRAY?

#print("__len__() return =", face_dataset.__len__())
print("__len__() return =", len(face_dataset))

for i in range(len(face_dataset)):
    sample = face_dataset[i]
    print(i, sample['mfccs'].shape, sample['landmarks'].shape)
    #print(i, sample['mfccs'], sample['landmarks'])

print("script complete!")
