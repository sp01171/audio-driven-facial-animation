# Library and function imports
import os
import pickle
import torch
import numpy as np
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
import matplotlib.pyplot as plt
from torchvision import transforms, utils
import torch.nn as nn
import torch.optim as optim
from math import ceil
import pprint
from torchvision.io import read_image

# Configuration for NN training
NUM_OF_EPOCHS = 2000
SIZE_OF_BATCHES = 1

# Configuration for dataset
START_CLIP = 2
END_CLIP = 19
VALIDATION_TRAINING_SPLIT = 0.2
SHUFFLE_VAL_TRAIN_SPLIT = True
TEST_START_CLIP = 20
TEST_END_CLIP = 20

# Configuration for evaluation plots
VISUAL_FRAME_NUM = 39
LANDMARK_SELECTION = 11

# Toggle for whether the program is in training or results viewing mode
TRAINING_MODE = 1

test_set_toggle = 0

"""Dataset functions"""


class FaceLandmarkMfccDataset(Dataset):
    """Face Landmarks Mfcc Dataset."""

    def __init__(self):
        """Create FaceLandmarkMfccDataset object"""

        self.cumulative_landmark_array = []
        self.cumulative_confidence_score_array = []
        self.cumulative_mfcc_array = []

        if (test_set_toggle == 0):
            clip_num = START_CLIP
            upper_clip_bound = END_CLIP
        elif (test_set_toggle == 1):
            clip_num = TEST_START_CLIP
            upper_clip_bound = TEST_END_CLIP
        else:
            print("Error - Upper and lower bounds for dataset are invalid!")
            exit()

        while (clip_num <= upper_clip_bound):
            print()
            print(clip_num)
            landmark_directory_path = "C:/GitHub/audio-driven-facial-animation/data/clip" + \
                str(clip_num) + "/landmarks/"
            mfcc_directory_path = "C:/GitHub/audio-driven-facial-animation/data/clip" + \
                str(clip_num) + "/audio/"

            """Load landmark frame arrays and mfcc lines"""

            # Creates a 2D array containing all landmarks,
            # with each row being an array containing frame landmarks from a different video clip (1-20),
            # with each of those arrays containing 68 landmarks
            single_clip_landmarks_and_confidence_scores, landmark_filecount = self.load_landmarks_and_confidence_scores(
                landmark_directory_path)
            print("landmark_filecount =", landmark_filecount)

            # Creates a 2D array containing all MFCCs,
            # with each row being an array containing frame MFCCs from a different video clip (1-20),
            # with each of those arrays containing 13 MFCCs
            single_clip_mfccs = self.load_mfccs(
                mfcc_directory_path, landmark_filecount)

            # 68 landmarks in an array, with dtype value
            # print(single_clip_landmarks_and_confidence_scores[frame_num][0])
            # 68 landmarks without dtype or outer array
            # print(single_clip_landmarks_and_confidence_scores[frame_num][0][0])
            # singular [x, y] landmark coord
            # print(single_clip_landmarks_and_confidence_scores[frame_num][0][0][0])
            # dtype value
            # print(single_clip_landmarks_and_confidence_scores[frame_num][0][1])
            # 68 landmark confidence scores in an array
            # print(single_clip_landmarks_and_confidence_scores[frame_num][1])
            frame_num = 0
            while (frame_num < landmark_filecount):
                # print("frame_num =", frame_num)
                # 68 landmarks without dtype or outer array
                # print(single_clip_landmarks_and_confidence_scores[frame_num][0][0])
                # 68 landmark confidence scores in an array
                # print(single_clip_landmarks_and_confidence_scores[frame_num][1][0])
                self.cumulative_landmark_array.append(
                    single_clip_landmarks_and_confidence_scores[frame_num][0][0])
                self.cumulative_confidence_score_array.append(
                    single_clip_landmarks_and_confidence_scores[frame_num][1][0])
                self.cumulative_mfcc_array.append(single_clip_mfccs[frame_num])
                frame_num += 1
            # print(self.cumulative_landmark_array[3])
            # print(self.cumulative_confidence_score_array[3])
            # print(type(self.cumulative_landmark_array))
            # print(type(self.cumulative_confidence_score_array))
            clip_num += 1

        self.cumulative_landmark_array = np.array(
            self.cumulative_landmark_array)
        self.cumulative_confidence_score_array = np.array(
            self.cumulative_confidence_score_array)
        # print(type(self.cumulative_landmark_array))
        # print(type(self.cumulative_confidence_score_array))
        # print(self.cumulative_landmark_array[3])
        # print(self.cumulative_confidence_score_array[3])
        print("\ncumulative_landmark_array =",
              self.cumulative_landmark_array.dtype)
        print("cumulative_confidence_score_array =",
              self.cumulative_confidence_score_array.dtype)
        self.cumulative_landmark_array = torch.from_numpy(
            self.cumulative_landmark_array)
        self.cumulative_confidence_score_array = torch.from_numpy(
            self.cumulative_confidence_score_array)
        # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        # self.cumulative_landmark_array = torch.tensor(self.cumulative_landmark_array)
        # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        # self.cumulative_confidence_score_array = torch.tensor(self.cumulative_confidence_score_array)
        # print(type(self.cumulative_landmark_array))
        # print(type(self.cumulative_confidence_score_array))
        # print(self.cumulative_landmark_array[3])
        # print(self.cumulative_confidence_score_array[3])
        print("cumulative_landmark_array =",
              self.cumulative_landmark_array.dtype)
        print("cumulative_confidence_score_array =",
              self.cumulative_confidence_score_array.dtype)

        # print(type(self.cumulative_mfcc_array))
        # print(self.cumulative_mfcc_array)
        self.cumulative_mfcc_array = np.array(
            self.cumulative_mfcc_array, dtype='float32')
        # print(type(self.cumulative_mfcc_array))
        # print(self.cumulative_mfcc_array)
        print("cumulative_mfcc_array =", self.cumulative_mfcc_array.dtype)
        self.cumulative_mfcc_array = torch.from_numpy(
            self.cumulative_mfcc_array)
        # Compiler notes that this conversion is very slow, and to use numpy as in-between...
        # self.cumulative_mfcc_array = torch.tensor(self.cumulative_mfcc_array)
        # print(type(self.cumulative_mfcc_array))
        # print(self.cumulative_mfcc_array)
        # print(self.cumulative_mfcc_array[146])
        print("cumulative_mfcc_array =", self.cumulative_mfcc_array.dtype)

        print("Number of samples = ", len(self.cumulative_landmark_array))

        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_landmark_array_vid2.txt",
                   self.cumulative_landmark_array[0], fmt='%0.1f')
        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_confidence_score_array.txt",
                   self.cumulative_confidence_score_array, fmt='%f')
        np.savetxt("C:/GitHub/audio-driven-facial-animation/dataloader stuff/np exports/cumulative_mfcc_array.txt",
                   self.cumulative_mfcc_array, fmt='%f')

        # Move tensor to the GPU if available
        if torch.cuda.is_available():
            self.cumulative_landmark_array = self.cumulative_landmark_array.to(
                'cuda')
            self.cumulative_confidence_score_array = self.cumulative_confidence_score_array.to(
                'cuda')
            self.cumulative_mfcc_array = self.cumulative_mfcc_array.to('cuda')
            print(
                f"Device tensors are stored on: {self.cumulative_mfcc_array.device}")

    def load_landmarks_and_confidence_scores(self, directory_in_str):
        """Load facial landmarks and confidence scores"""

        landmark_and_confidence_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".p"):
                # print(filename)
                # print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                # print(filepath)
                # LOAD PICKLE FILE & APPEND INTO ARRAY
                # With-statement causes file to close as soon as the indented code below is executed
                with open(filepath, "rb") as f:
                    landmark_and_confidence_array.append(pickle.load(f))
                # print(landmark_array)
                counter += 1
                # print("counter =", counter)
            else:
                continue
        # print(landmark_array)
        # print("counter =", counter)
        return landmark_and_confidence_array, counter

    def load_mfccs(self, directory_in_str, local_landmark_filecount):
        """Load MFCC files"""

        mfcc_array = []
        counter = 0
        directory = os.fsencode(directory_in_str)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".npy") and counter < local_landmark_filecount:
                # print(filename)
                # print(directory_in_str + filename)
                filepath = os.path.join(directory_in_str, filename)
                # print(filepath)
                # LOAD .npy file & APPEND INTO ARRAY
                mfcc_array.append(np.load(filepath))
                # print(mfcc_array)
                counter += 1
            elif counter < local_landmark_filecount:
                # print("Non npy file found.")
                continue
            else:
                print("Frame count matched, breaking loop...")
                break
        print("counter =", counter)
        print("local_landmark_filecount =", local_landmark_filecount)
        return mfcc_array

    def __len__(self):
        """Return length of input data"""

        return len(self.cumulative_mfcc_array)

    def __getitem__(self, idx):
        """Return sample of dataset"""

        # If idx is a tensor
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = {'mfccs': self.cumulative_mfcc_array[idx], 'landmarks': self.cumulative_landmark_array[idx],
                  'confidence_scores': self.cumulative_confidence_score_array[idx]}
        return sample


################################################################
"""Plotting functions"""


def predict_test_image():
    """Predict landmarks for test image"""

    # Now tests on the chosen frame of the test set
    img_tensor_list = []  # List of tensors
    new_frame_path = ('C:/GitHub/audio-driven-facial-animation/data/clip' + str(
        TEST_START_CLIP) + '/images/image' + str(VISUAL_FRAME_NUM).zfill(4) + '.jpg')
    print("new_frame_path =", new_frame_path)
    img_tensor = read_image(new_frame_path)
    # print("img_tensor =\n", img_tensor)
    # print(type(img_tensor))
    img_tensor_list.append(img_tensor)
    # Converts the list of tensors into a larger tensor
    img_batch = torch.stack(img_tensor_list)
    # print("img_batch type = \n", type(img_batch))

    dataiter = iter(testloader)
    sample = next(dataiter)
    mfccs = sample['mfccs']
    landmarks = sample['landmarks']
    # print("\nmfccs =\n", mfccs)
    print("\nlandmarks =\n", landmarks)
    output_tensor = net(mfccs)
    print("\noutput_tensor =\n", output_tensor)
    # print("\noutput_tensor.size() =\n", output_tensor.size())

    return img_batch, landmarks, output_tensor


def plot_landmarks(img_batch, landmarks, output_tensor):
    """Plot facial landmarks for test image"""
    organised_output = []
    organised_output_list = []
    i = 0
    while i < 136:
        if (i % 2) == 0:  # checks if even
            # organised_output.append([round(output_tensor[0][i].item(), 1), round(output_tensor[0][i+1].item(), 1)])
            organised_output.append(
                [output_tensor[0][i].item(), output_tensor[0][i+1].item()])
        i += 2
        # print(i)
    organised_output_list.append(organised_output)
    # print("\norganised_output_list=")
    # pprint.pprint(organised_output_list)

    # Show image with landmarks for a batch of samples.
    # .cpu() copies the landmark tensors back onto system memory, so it can be used in the code below
    images_batch, landmarks_batch = img_batch.cpu(), landmarks.cpu()
    batch_size = len(images_batch)
    print("len(images_batch) =", batch_size)
    im_size = images_batch.size(2)  # Don't know why it's size(2)...
    # print("im_size = images_batch.size(2) =", im_size)
    # grid_border_size = 2
    # We're only displaying one image at a time, so there is no border for subsequent images in the grid!
    grid_border_size = 0
    # print("i_batch val =", i_batch)
    grid = utils.make_grid(images_batch)
    plt.imshow(grid.numpy().transpose((1, 2, 0)))
    for i in range(batch_size):
        landmark_x_val = landmarks_batch[i, :, 0].numpy(
        ) + i * im_size + (i + 1) * grid_border_size
        landmark_y_val = landmarks_batch[i, :, 1].numpy() + grid_border_size
        # print("landmarks_batch[i, :, 0].numpy() =", landmarks_batch[i, :, 0].numpy())
        # print("landmarks_batch[i, :, 1].numpy() =", landmarks_batch[i, :, 1].numpy())
        plt.scatter(landmark_x_val,
                    landmark_y_val,
                    s=70, marker='.', c='r')
        prediction_x_val = np.array(organised_output_list)[
            i, :, 0] + i * im_size + (i + 1) * grid_border_size
        prediction_y_val = np.array(organised_output_list)[
            i, :, 1] + grid_border_size
        # print("np.array(organised_output_list)[i, :, 0] =", np.array(organised_output_list)[i, :, 0])
        # print("np.array(organised_output_list)[i, :, 1] =", np.array(organised_output_list)[i, :, 1])
        plt.scatter(prediction_x_val,
                    prediction_y_val,
                    s=10, marker='.', c='b')
        plt.title("Test Image: clip" + str(TEST_START_CLIP) + "/images/image" + str(VISUAL_FRAME_NUM).zfill(4) + ".jpg\n" +
                  "Batch Size: " + str(SIZE_OF_BATCHES) + ", Samples: " + str(train_split) + ", Epochs: " + str(NUM_OF_EPOCHS))
        plt.axis('off')


def calculate_mean_test_loss():
    """Calculate mean loss for all test image predictions and per image"""

    cumulative_test_loss = 0
    running_test_loss = 0.0
    mean_test_frame_loss_arr = []
    print("len(testloader) =", len(testloader))
    for i, data in enumerate(testloader, 0):
        input = data['mfccs']
        target = data['landmarks']
        # flatten all dimensions except batch
        target = torch.flatten(target, 1)
        output = net(input)
        loss = criterion(output, target)
        running_test_loss += loss.item()
        # print(i)
        # print("running_test_loss =", running_test_loss)
        cumulative_test_loss += running_test_loss
        mean_test_frame_loss_arr.append(running_test_loss)
        running_test_loss = 0.0
    mean_test_loss = cumulative_test_loss / len(testloader)
    return mean_test_loss, mean_test_frame_loss_arr


def plot_loss_against_epochs(epoch_losses_arr, val_epoch_losses_arr, mean_test_loss):
    """Plot mean loss of training and validation dataset, over each training epoch"""

    print("\nepoch_losses =\n", epoch_losses_arr)
    print("\nval_epoch_losses =\n", val_epoch_losses_arr)
    print()
    print(len(epoch_losses_arr))
    print(max(epoch_losses_arr))
    print(ceil(max(epoch_losses_arr)/10)*10)
    print(len(val_epoch_losses_arr))
    print(max(val_epoch_losses_arr))
    print(ceil(max(val_epoch_losses_arr)/10)*10)

    plt.figure()
    plt.plot(np.array(epoch_losses_arr), color='r', label="Training")
    plt.plot(np.array(val_epoch_losses_arr), color='g', label="Validation")
    plt.xlim(0, len(epoch_losses_arr))
    plt.ylim(0, (ceil(max(epoch_losses_arr)/10)*10))
    plt.title("Final Training Loss: " + str(round(epoch_losses_arr[-1], 1)) + ", Mean Testing Loss: " + str(round(
        mean_test_loss, 1)) + "\nLoss against Epochs" + " (Batch Size: " + str(SIZE_OF_BATCHES) + ", Epochs: " + str(NUM_OF_EPOCHS) + ")")
    plt.xlabel("Epochs")
    plt.ylabel("Mean Loss (L1 Norm)")
    plt.legend()


def plot_mean_test_frame_errors(mean_test_frame_loss_arr):
    """Individually plot mean loss for each test frame (per image)"""

    print()
    print(len(mean_test_frame_loss_arr))
    print(max(mean_test_frame_loss_arr))
    print(ceil(max(mean_test_frame_loss_arr)/10)*10)
    plt.figure()
    frames = range(136)
    plt.bar(frames, mean_test_frame_loss_arr, color='maroon', width=0.4)
    plt.xlim(-1, len(mean_test_frame_loss_arr))
    plt.ylim(0, (ceil(max(mean_test_frame_loss_arr)/10)*10))
    plt.title("Mean error for all frames in testing dataset")
    plt.xlabel("Frame")
    plt.ylabel("Mean loss (L1 Norm)")


def calculate_individual_landmark_test_losses():
    """Calculate individual loss for each landmark, in every test image"""

    criterion_individual = nn.L1Loss(reduction='none')
    criterion_mean = nn.L1Loss(reduction='mean')
    individual_landmark_loss_arr_list = []
    # print("len(testloader) =", len(testloader))
    for i, data in enumerate(testloader, 0):
        input = data['mfccs']
        target = data['landmarks']
        # flatten all dimensions except batch
        target = torch.flatten(target, 1)
        output = net(input)
        loss_individual = criterion_individual(output, target)
        loss_mean = criterion_mean(output, target)
        # print("\nloss_individual.size() = ", loss_individual.size(), "\n")
        # print("loss_individual =\n", loss_individual, "\n")
        # print("loss_mean =\n", loss_mean, "\n")

        individual_loss_arr = []
        j = 0
        while j < 136:
            if (j % 2) == 0:  # checks if even
                # individual_loss_arr.append(loss_individual[0][j].item() + loss_individual[0][j+1].item())
                individual_loss_arr.append((loss_individual[0][j].item(
                ) + loss_individual[0][j+1].item()) / 2)  # CHECK THISSSSSSSSSS
            j += 2
            # print(j)
        individual_landmark_loss_arr_list.append(individual_loss_arr)

    # print("\nlen(individual_landmark_loss_arr_list[1]) = ", len(individual_landmark_loss_arr_list[0]))
    # print("\nindividual_landmark_loss_arr_list = \n", individual_landmark_loss_arr_list, "\n")

    return individual_landmark_loss_arr_list


def plot_individual_landmark_deviations(individual_landmark_loss_arr_list):
    """Individually plot mean and standard deviation for 68 facial landmarks, across all 136 test frames"""

    # Divide the arr_list into 68 lists for each landmark? Maybe unneccessary...
    # Create a list of the means of each landmark
    # Create a list of the standard deviations of each landmark

    mean_landmark_loss_arr = []
    ordered_landmark_loss_arr = []
    landmark_loss_std_arr = []
    i = 0
    # Should iterate 68 times
    while (i < len(individual_landmark_loss_arr_list[0])):
        landmark_loss_arr = []
        # Appends all landmarks for position # into an array
        for arr in individual_landmark_loss_arr_list:
            landmark_loss_arr.append(arr[i])
        """if (i == 0): # First loop
            print("\n", individual_landmark_loss_arr_list[0][0])
            print(individual_landmark_loss_arr_list[1][0])
            print("landmark_loss_total = \n", landmark_loss_total, "\n")"""
        # ordered_landmark_loss_arr contains all losses ordered by landmark,
        # in sub arrays - VERY IMPORTANT INFORMATION
        ordered_landmark_loss_arr.append(landmark_loss_arr)
        # Calculate standard deviation
        landmark_loss_std = np.std(np.array(landmark_loss_arr))
        landmark_loss_std_arr.append(landmark_loss_std)
        # Calculate mean average
        mean_landmark_loss = sum(landmark_loss_arr) / len(landmark_loss_arr)
        mean_landmark_loss_arr.append(mean_landmark_loss)
        i += 1
    print("mean_landmark_loss_arr = \n",
          mean_landmark_loss_arr, "\n")  # PLOT THIS ARRAY!
    print("len(landmark_loss_std_arr) = \n", len(landmark_loss_std_arr), "\n")
    print("landmark_loss_std_arr = \n", landmark_loss_std_arr, "\n")
    plt.figure()
    plt.errorbar(range(68), mean_landmark_loss_arr, landmark_loss_std_arr,
                 linestyle='None', capsize=3, fmt='o')  # , marker='^')
    plt.title("Mean landmark loss for test set")
    plt.xlabel("Landmark Number")
    plt.ylabel("Mean loss and standard deviation (L1 Norm)")

    # Returns the 2D array so that it can be used in eval plot 3!
    # ordered_landmark_loss_arr[landmark number 0-67][frame number 0-135] -> loss value
    return ordered_landmark_loss_arr


# pred_types = {'face': pred_type(slice(0, 17), (0.682, 0.780, 0.909, 0.5)),
#               'eyebrow1': pred_type(slice(17, 22), (1.0, 0.498, 0.055, 0.4)),
#               'eyebrow2': pred_type(slice(22, 27), (1.0, 0.498, 0.055, 0.4)),
#               'nose': pred_type(slice(27, 31), (0.345, 0.239, 0.443, 0.4)),
#               'nostril': pred_type(slice(31, 36), (0.345, 0.239, 0.443, 0.4)),
#               'eye1': pred_type(slice(36, 42), (0.596, 0.875, 0.541, 0.3)),
#               'eye2': pred_type(slice(42, 48), (0.596, 0.875, 0.541, 0.3)),
#               'lips': pred_type(slice(48, 60), (0.596, 0.875, 0.541, 0.3)),
#               'teeth': pred_type(slice(60, 68), (0.596, 0.875, 0.541, 0.4))
#               }


def plot_single_landmark_losses(ordered_landmark_loss_arr, landmark_selection):
    """Plot loss for specified landmark individually over all test frames"""

    plt.figure()
    frames = range(136)
    plt.bar(
        frames, ordered_landmark_loss_arr[landmark_selection], color='olive', width=0.4)
    plt.xlim(-1, len(ordered_landmark_loss_arr[landmark_selection]))
    plt.ylim(
        0, (ceil(max(ordered_landmark_loss_arr[landmark_selection])/10)*10))
    plt.title("Landmark " + str(landmark_selection) +
              ": Loss for all frames in testing dataset")
    plt.xlabel("Frame")
    plt.ylabel("Loss (L1 Norm)")


###########################################
"""Main program script"""

# plt.ion()   # interactive mode
training_and_validation_dataset = FaceLandmarkMfccDataset()
print("Training & Validation Data loaded successfully!")
# print("__len__() return =", training_and_validation_dataset.__len__())
print("__len__() return =", len(training_and_validation_dataset), "\n")
test_set_toggle = 1
test_dataset = FaceLandmarkMfccDataset()
print("Testing Dataset loaded successfully!")
print("__len__() return =", len(test_dataset), "\n")

# Split training and validation dataset
validation_split = VALIDATION_TRAINING_SPLIT
print("validation_split =", validation_split)
shuffle_dataset = SHUFFLE_VAL_TRAIN_SPLIT
# random_seed = 42
dataset_size = len(training_and_validation_dataset)
print("dataset_size =", dataset_size)
indices = list(range(dataset_size))
split = int(np.floor(validation_split * dataset_size))
train_split = int(dataset_size - split)
print("split =", split)
print("train_split =", train_split)
if shuffle_dataset:
    # np.random.seed(random_seed)
    np.random.shuffle(indices)
train_indices, val_indices = indices[split:], indices[:split]
print("train_indices =", train_indices)
print("val_indices =", val_indices)

# Create data samplers and data loaders
train_sampler = SubsetRandomSampler(train_indices)
valid_sampler = SubsetRandomSampler(val_indices)
trainloader = DataLoader(training_and_validation_dataset, batch_size=SIZE_OF_BATCHES,
                        shuffle=False, sampler=train_sampler, num_workers=0)
print("Trainloader created successfully!")
valloader = DataLoader(training_and_validation_dataset, batch_size=SIZE_OF_BATCHES,
                    shuffle=False, sampler=valid_sampler, num_workers=0)
print("Valloader created successfully!")
# testloader = DataLoader(test_dataset, batch_size=SIZE_OF_BATCHES, shuffle=False, num_workers=0)
testloader = DataLoader(test_dataset, batch_size=1,
                        shuffle=False, num_workers=0)
print("Testloader created successfully!\n")

# Define neural network

# [4, 13] -> [4, 68, 2], where batch size is 4
# [13 x 1] -> [(68 x 2) x 1]
# Fully Connected Layer


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(13 * 1, 136 * 1)

    def forward(self, x):
        output = self.fc1(x)
        return output


net = Net().to('cuda')
if torch.cuda.is_available():
    net = net.to('cuda')
    print("Net() successfully loaded onto CUDA!")
criterion = nn.L1Loss()  # L1 Norm
# Stochastic Gradient Descent
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
#optimizer = optim.SGD(net.parameters(), lr=0.0005, momentum=0.9)


if (TRAINING_MODE == True):
    epoch_losses = []
    val_epoch_losses = []
    print("len(trainloader) =", len(trainloader))
    print("len(valloader) =", len(valloader))

    # Neural network training loop
    for epoch in range(NUM_OF_EPOCHS):
        cumulative_loss = 0
        running_loss = 0.0
        # Iterate over training data and perform backprop
        for i, data in enumerate(trainloader, 0):
            input = data['mfccs']
            target = data['landmarks']
            # flatten all dimensions except batch
            target = torch.flatten(target, 1)
            # zero the parameter gradients
            optimizer.zero_grad()
            # forward + backward + optimize
            output = net(input)
            loss = criterion(output, target)
            loss.backward()
            optimizer.step()
            # print statistics
            running_loss += loss.item()
            epoch_size = len(trainloader)
            if i % epoch_size == (epoch_size-1):    # print every epoch
                print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss:.3f}')
            cumulative_loss += running_loss
            running_loss = 0.0

        # Without dividing the batch size, it gives us the loss for each batch,
        # when dividing is gives us the loss for each epoch(?)
        # epoch_losses.append(cumulative_loss / SIZE_OF_BATCHES)
        epoch_losses.append(cumulative_loss / len(trainloader))

        # Calculate losses for validation dataset after each training epoch
        cumulative_val_loss = 0
        running_val_loss = 0.0
        for i, data in enumerate(valloader, 0):
            input = data['mfccs']
            target = data['landmarks']
            # flatten all dimensions except batch
            target = torch.flatten(target, 1)
            output = net(input)
            loss = criterion(output, target)
            running_val_loss += loss.item()
            cumulative_val_loss += running_val_loss
            running_val_loss = 0.0
        val_epoch_losses.append(cumulative_val_loss / len(valloader))

    # print("\nval_epoch_losses =\n", val_epoch_losses, "\n")

    print('Finished Training')

    # Save training results
    epoch_losses_path = "./ADFA_net_batchsize" + str(SIZE_OF_BATCHES) + "_epochs" + str(NUM_OF_EPOCHS) + "_epochlosses" + ".p"
    with open(epoch_losses_path, "wb") as f:
                    pickle.dump(epoch_losses, f)
    val_epoch_losses_path = "./ADFA_net_batchsize" + str(SIZE_OF_BATCHES) + "_epochs" + str(NUM_OF_EPOCHS) + "_valepochlosses" + ".p"
    with open(val_epoch_losses_path, "wb") as g:
                    pickle.dump(val_epoch_losses, g)
    
    # Save trained neural network and training results
    PATH = "./ADFA_net_batchsize" + str(SIZE_OF_BATCHES) + "_epochs" + str(NUM_OF_EPOCHS) + ".pth"
    torch.save(net.state_dict(), PATH)
    print('Trained Neural Network Saved')


if (TRAINING_MODE == False):
    
    # Load training results
    epoch_losses_path = "./ADFA_net_batchsize" + str(SIZE_OF_BATCHES) + "_epochs" + str(NUM_OF_EPOCHS) + "_epochlosses" + ".p"
    with open(epoch_losses_path, "rb") as f:
                    epoch_losses = pickle.load(f)
    val_epoch_losses_path = "./ADFA_net_batchsize" + str(SIZE_OF_BATCHES) + "_epochs" + str(NUM_OF_EPOCHS) + "_valepochlosses" + ".p"
    with open(val_epoch_losses_path, "rb") as g:
                    val_epoch_losses = pickle.load(g)

    # Load trained neural network
    net = Net().to('cuda')
    PATH = "./ADFA_net_batchsize" + str(SIZE_OF_BATCHES) + "_epochs" + str(NUM_OF_EPOCHS) + ".pth"
    net.load_state_dict(torch.load(PATH))
    print('Trained Neural Network Loaded')

    """Plotting function calls"""

    plt.ion()   # interactive mode

    # Predict landmarks for test image
    img_batch, landmarks, output_tensor = predict_test_image()

    # Calculate mean loss for all test image predictions and per image
    mean_test_loss, mean_test_frame_loss_arr = calculate_mean_test_loss()
    print("\nmean_test_loss =", mean_test_loss, "\n")
    print("\nmean_test_frame_loss_arr =", mean_test_frame_loss_arr, "\n")

    # Calculate individual loss for each landmark, in every test image
    individual_landmark_loss_arr_list = calculate_individual_landmark_test_losses()

    # Plot facial landmarks for test image
    plot_landmarks(img_batch, landmarks, output_tensor)

    # Plot mean loss of training and validation dataset, over each training epoch
    plot_loss_against_epochs(epoch_losses, val_epoch_losses, mean_test_loss)

    # Individually plot mean loss for each test frame (per image)
    plot_mean_test_frame_errors(mean_test_frame_loss_arr)

    # Individually plot mean and standard deviation for 68 facial landmarks, across all 136 test frames
    ordered_landmark_loss_arr = plot_individual_landmark_deviations(
        individual_landmark_loss_arr_list)

    # Plot loss for specified landmark individually over all test frames
    plot_single_landmark_losses(ordered_landmark_loss_arr, LANDMARK_SELECTION)

    plt.ioff()
    # plt.legend()
    plt.show()

print("script complete!")
