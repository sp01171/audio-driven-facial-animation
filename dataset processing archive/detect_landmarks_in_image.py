import face_alignment

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from skimage import io
import collections
import numpy as np
import cv2

# Optionally set detector and some additional detector parameters
face_detector = 'sfd'
face_detector_kwargs = {
    "filter_threshold" : 0.8
}

# Run the 2D face alignment on a test image, without CUDA.
fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, device='cpu', flip_input=True,
                                  face_detector=face_detector, face_detector_kwargs=face_detector_kwargs, verbose = True)
"""fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, device='cuda', flip_input=True,
                                  face_detector=face_detector, face_detector_kwargs=face_detector_kwargs, verbose = True)"""

try:
    #input_img = io.imread('image0140.jpg')
    #input_img = io.imread('blinking.jpg')
    input_img = io.imread('angledshot.jpg')
except FileNotFoundError:
    #input_img = io.imread('image0140.jpg')
    #input_img = io.imread('blinking.jpg')
    input_img = io.imread('angledshot.jpg')

"""Downscale"""

dimensions = input_img.shape
# print(dimensions)
y = dimensions[0]
x = dimensions[1]
#print(y, x)
# Assumes the image is never taller than it is wide
if (y > 1920):
    pixel_y_scale = 1080 / y
    # Downscale frame to 1920x1080 while maintaining vertical FOV
    img = cv2.resize(input_img, None, fx=pixel_y_scale,
                        fy=pixel_y_scale, interpolation=cv2.INTER_AREA)

"""Rotate"""

preds = fa.get_landmarks(input_img)[-1]

print("preds printing:")
print(preds)

# 2D-Plot
plot_style = dict(marker='o',
                  markersize=4,
                  linestyle='-',
                  lw=2)

# Each slice object value range corresponds to a different part of the detected facial features! e.g. face, nose, lips, etc.
# But printing the 2D preds list of tuples, these values don't seem to appear!!!
# Unless these correspond to the order in which the tuples appear in the list, as they don't appear to be sorted by any sort of set value!
# OKAY THAT APPEARS TO BE THE CASE!!!
# So a total of 68 detected points! with 9 separate markers
# I could calculate the co-ords of the centre of each eye, then I could use the existing triangle angle method to calculate the current angle of the face/image, and then rotate it like before...

pred_type = collections.namedtuple('prediction_type', ['slice', 'color'])
pred_types = {'face': pred_type(slice(0, 17), (0.682, 0.780, 0.909, 0.5)),
              'eyebrow1': pred_type(slice(17, 22), (1.0, 0.498, 0.055, 0.4)),
              'eyebrow2': pred_type(slice(22, 27), (1.0, 0.498, 0.055, 0.4)),
              'nose': pred_type(slice(27, 31), (0.345, 0.239, 0.443, 0.4)),
              #'nose': pred_type(slice(27, 30), (0.345, 0.239, 0.443, 0.4)),
              #'nosebottom': pred_type(slice(31, 31), (0.345, 0.239, 0.443, 0.4)),
              'nostril': pred_type(slice(31, 36), (0.345, 0.239, 0.443, 0.4)),
              'eye1': pred_type(slice(36, 42), (0.596, 0.875, 0.541, 0.3)),
              'eye2': pred_type(slice(42, 48), (0.596, 0.875, 0.541, 0.3)),
              'lips': pred_type(slice(48, 60), (0.596, 0.875, 0.541, 0.3)),
              'teeth': pred_type(slice(60, 68), (0.596, 0.875, 0.541, 0.4))
              }

# Marker 8 is chin, marker 30 is nose, marker 27 is the nose bridge
# Preds tuples are in the format (x, y)
# If nose_x - chin_x is positive, then the face is tilted right, therefore rotation should be anticlockwise…
# If nose_x - chin_x is negative, then face is tilted left, therefore rotation should be clockwise…
# Actually it doesn't matter, since the angle calculation will give us a negative angle, so the rotation will be accurate no matter what!


# Calculate angle triangle
print("nose bridge tuple is", preds[27])
print("nose tuple is", preds[30])
print("chin tuple is", preds[8])

delta_x = preds[27][0] - preds[8][0]
print("delta_x =", delta_x)
# Note that the calculation is inverted due to the y-axis being inverted, co-ords are taken from the top left corner of the image!!!
delta_y = preds[8][1] - preds[27][1]
print("delta_y =", delta_y)
angle = np.arctan(delta_x/delta_y)
angle = ((angle * 180) / np.pi)
#angle = (angle * 180) / np.pi
print("Rotation angle in degrees is " + str(angle))

h, w = input_img.shape[:2]
#print("img: h = ", h, "w = ", w)

# Calculate centre point of the image
# Integer division "//"" ensures that we receive whole numbers
#center = (w // 2, h // 2)

# Defining a matrix M and calling
# cv2.getRotationMatrix2D method
M = cv2.getRotationMatrix2D(preds[27], (angle), 1.0)
# Applying the rotation to our image using the
# cv2.warpAffine method
rotated = cv2.warpAffine(input_img, M, (w, h))

preds_rotated = fa.get_landmarks(rotated)[-1]

fig = plt.figure(figsize=plt.figaspect(.5))
ax = fig.add_subplot(1, 2, 1)
#ax.imshow(input_img)
ax.imshow(rotated)

"""for pred_type in pred_types.values():
    ax.plot(preds[pred_type.slice, 0],
            preds[pred_type.slice, 1],
            color=pred_type.color, **plot_style)"""

for pred_type in pred_types.values():
    ax.plot(preds_rotated[pred_type.slice, 0],
            preds_rotated[pred_type.slice, 1],
            color=pred_type.color, **plot_style)

ax.axis('off')
plt.show()

"""
# 3D-Plot
ax = fig.add_subplot(1, 2, 2, projection='3d')
surf = ax.scatter(preds[:, 0] * 1.2,
                  preds[:, 1],
                  preds[:, 2],
                  c='cyan',
                  alpha=1.0,
                  edgecolor='b')

for pred_type in pred_types.values():
    ax.plot3D(preds[pred_type.slice, 0] * 1.2,
              preds[pred_type.slice, 1],
              preds[pred_type.slice, 2], color='blue')

ax.view_init(elev=90., azim=90.)
ax.set_xlim(ax.get_xlim()[::-1])
plt.show()
"""

 """Crop"""

 """Scale"""