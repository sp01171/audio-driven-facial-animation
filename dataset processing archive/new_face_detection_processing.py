import face_alignment

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from skimage import io
import collections
import numpy as np
import cv2

# Optionally set detector and some additional detector parameters
face_detector = 'sfd'
face_detector_kwargs = {
    "filter_threshold" : 0.8
}

# Run the 2D face alignment on a test image, without CUDA.
fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, device='cpu', flip_input=True,
                                  face_detector=face_detector, face_detector_kwargs=face_detector_kwargs, verbose = True)
"""fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, device='cuda', flip_input=True,
                                  face_detector=face_detector, face_detector_kwargs=face_detector_kwargs, verbose = True)"""

try:
    input_img = io.imread('blinking.jpg')
    #input_img = io.imread('angledshot.jpg')
    #input_img = io.imread('vid1frame1snapshot.jpg')
    #input_img = io.imread('bboxtest.jpg')
except FileNotFoundError:
    input_img = io.imread('blinking.jpg')
    #input_img = io.imread('angledshot.jpg')
    #input_img = io.imread('vid1frame1snapshot.jpg')
    #input_img = io.imread('bboxtest.jpg')

"""Downscale"""

dimensions = input_img.shape
print(dimensions)
y = dimensions[0]
x = dimensions[1]
#print(y, x)
# Assumes the image is never taller than it is wide
# Always scale image to 1080p vertical resolution, regardless of the horizontal resolution / aspect ratio
if (y != 1080):
    pixel_y_scale = 1080 / y
    # Downscale frame to 1920x1080 while maintaining vertical FOV / aspect ratio
    input_img = cv2.resize(input_img, None, fx=pixel_y_scale,
                        fy=pixel_y_scale, interpolation=cv2.INTER_AREA)
print(input_img.shape)

"""Rotate"""

#preds = fa.get_landmarks(input_img)[-1]
preds = fa.get_landmarks_from_image(input_img)[-1]

print("preds printing:")
print(preds)

# 2D-Plot
plot_style = dict(marker='o',
                  markersize=4,
                  linestyle='-',
                  lw=2)

# Each slice object value range corresponds to a different part of the detected facial features! e.g. face, nose, lips, etc.
# But printing the 2D preds list of tuples, these values don't seem to appear!!!
# Unless these correspond to the order in which the tuples appear in the list, as they don't appear to be sorted by any sort of set value!
# OKAY THAT APPEARS TO BE THE CASE!!!
# So a total of 68 detected points! with 9 separate markers
# I could calculate the co-ords of the centre of each eye, then I could use the existing triangle angle method to calculate the current angle of the face/image, and then rotate it like before...

pred_type = collections.namedtuple('prediction_type', ['slice', 'color'])
pred_types = {'face': pred_type(slice(0, 17), (0.682, 0.780, 0.909, 0.5)),
              'eyebrow1': pred_type(slice(17, 22), (1.0, 0.498, 0.055, 0.4)),
              'eyebrow2': pred_type(slice(22, 27), (1.0, 0.498, 0.055, 0.4)),
              'nose': pred_type(slice(27, 31), (0.345, 0.239, 0.443, 0.4)),
              #'nose': pred_type(slice(27, 30), (0.345, 0.239, 0.443, 0.4)),
              #'nosebottom': pred_type(slice(31, 31), (0.345, 0.239, 0.443, 0.4)),
              'nostril': pred_type(slice(31, 36), (0.345, 0.239, 0.443, 0.4)),
              'eye1': pred_type(slice(36, 42), (0.596, 0.875, 0.541, 0.3)),
              'eye2': pred_type(slice(42, 48), (0.596, 0.875, 0.541, 0.3)),
              'lips': pred_type(slice(48, 60), (0.596, 0.875, 0.541, 0.3)),
              'teeth': pred_type(slice(60, 68), (0.596, 0.875, 0.541, 0.4))
              }

# Marker 8 is chin, marker 30 is nose, marker 27 is the nose bridge
# Preds tuples are in the format (x, y)
# If nose_x - chin_x is positive, then the face is tilted right, therefore rotation should be anticlockwise…
# If nose_x - chin_x is negative, then face is tilted left, therefore rotation should be clockwise…
# Actually it doesn't matter, since the angle calculation will give us a negative angle, so the rotation will be accurate no matter what!


# Calculate angle triangle
print("nose bridge tuple is", preds[27])
print("nose tuple is", preds[30])
print("chin tuple is", preds[8])

delta_x = preds[27][0] - preds[8][0]
print("delta_x =", delta_x)
# Note that the calculation is inverted due to the y-axis being inverted, co-ords are taken from the top left corner of the image!!!
delta_y = preds[8][1] - preds[27][1]
print("delta_y =", delta_y)
angle = np.arctan(delta_x/delta_y)
angle = ((angle * 180) / np.pi)
#angle = (angle * 180) / np.pi
print("Rotation angle in degrees is " + str(angle))

h, w = input_img.shape[:2]
#print("img: h = ", h, "w = ", w)

# Calculate centre point of the image
# Integer division "//"" ensures that we receive whole numbers
#center = (w // 2, h // 2)

# Defining a matrix M and calling
# cv2.getRotationMatrix2D method
M = cv2.getRotationMatrix2D(preds[27], (angle), 1.0)
# Applying the rotation to our image using the
# cv2.warpAffine method
#NOT SURE WHETHER THE W AND H VALUES ARE IN THE CORRECT ARGUMENT ORDER HERE!!!
rotated = cv2.warpAffine(input_img, M, (w, h))

#preds_rotated = fa.get_landmarks(rotated)[-1]
#preds_rotated = fa.get_landmarks_from_image(rotated)[-1]
#preds_rotated, landmark_score, detected_bbox = fa.get_landmarks_from_image(rotated, detected_faces=None, return_bboxes=True, return_landmark_score=False)[-1]
preds_rotated, landmark_score, detected_bbox = fa.get_landmarks_from_image(rotated, detected_faces=None, return_bboxes=True, return_landmark_score=False)
# Obtains just the array from the list, removing extra info about float value of the array (e.g. dtype=float32)
preds_rotated = preds_rotated[-1]
# Extracts the bbox array from the list
detected_bbox = detected_bbox[0]

print("preds_rotated: ", preds_rotated)
print("landmark_score: ", landmark_score)
print("detected_bbox: ", detected_bbox)

"""ADD CODE TO OBTAIN BBOX HERE!!! CHANGE CODE BELOW TO CROP AROUND THE BBOX, OR MAYBE THE BBOX + SOME EXTRA PADDING AROUND IT TO INCLUDE HAIR?"""

print("rotation done!")

"""Crop"""

"""# Using nose bridge marker from 2nd pass, as the centre point of the image

#centre_x = preds[27][0]
#centre_y = preds[27][1]
crop_size = 700

crop_square_x_min = int(preds_rotated[27][0] - (crop_size/2))
crop_square_x_max = int(preds_rotated[27][0] + (crop_size/2))

crop_square_y_min = int(preds_rotated[27][1] - (crop_size/2))
crop_square_y_max = int(preds_rotated[27][1] + (crop_size/2))

print("y =", y)
print("crop_size =", crop_size)

print("preds_rotated[27][0] =", preds_rotated[27][0])
print("preds_rotated[27][1] =", preds_rotated[27][1])

print("crop_square_x_min =", crop_square_x_min)
print("crop_square_x_max =", crop_square_x_max)
print("crop_square_y_min =", crop_square_y_min)
print("crop_square_y_max =", crop_square_y_max)

cropped = rotated[crop_square_y_min:crop_square_y_max, crop_square_x_min:crop_square_x_max]"""

# Bbox co-ords are in the format (x, y)
"""bbox_top_left = (detected_bbox[0], detected_bbox[1])
bbox_top_right = (detected_bbox[2], detected_bbox[1])
bbox_bottom_left = (detected_bbox[0], detected_bbox[3])
bbox_bottom_right = (detected_bbox[2], detected_bbox[3])
print("bbox_top_left: ", bbox_top_left)
print("bbox_top_right: ", bbox_top_right)
print("bbox_bottom_left: ", bbox_bottom_left)
print("bbox_bottom_right: ", bbox_bottom_right)"""

#NOT SURE WHETHER TO DO EXPLICIT INT CONVERSION OR TO USE THE CEIL() FUNCTION FROM THE MATH LIBRARY, WHICH WILL ALWAYS ROUND UP!!!
#IF FACE IS TILTED IN THE INPUT IMAGE, THE BBOX MIGHT CUT OFF FACE POINTS/MARKERS!!!
bbox_x_min = int(detected_bbox[0])
bbox_y_min = int(detected_bbox[1])
bbox_x_max = int(detected_bbox[2])
bbox_y_max = int(detected_bbox[3])

bbox_delta_x = bbox_x_max - bbox_x_min
bbox_delta_y = bbox_y_max - bbox_y_min
print("bbox_delta_x: ", bbox_delta_x)
print("bbox_delta_y: ", bbox_delta_y)
if bbox_delta_x <= 0 or bbox_delta_y <= 0:
    raise Exception("Error! Bbox co-ordinates are invalid or in the incorrect order")

# Check which bbox dimension is largest
"""bbox_crop_size = int(max(bbox_delta_x, bbox_delta_y))
print("bbox_crop_size:", bbox_crop_size)
bbox_crop_size_padded = bbox_crop_size + 50
print("bbox_crop_size_padded:", bbox_crop_size_padded)"""
# If delta is positive then y > x, if delta is negative then x > y
# Bbox height is usually greater than width, due to face shapes being taller than than they are wide
bbox_dimension_delta = bbox_delta_y - bbox_delta_x
print("bbox_dimension_delta =", bbox_dimension_delta)
#bbox_padding_value = 50
bbox_padding_value = 0
# Even if expanding a dimension causes .5 of a pixel to be added to either side (min and max values), then upon the second int() conversion they will both be rounded up, so the difference between them will remain the same!
# If y > x, expand the x-value of the bbox and add 50 pixels of padding
if bbox_dimension_delta > 0:
    bbox_x_min_adjusted = detected_bbox[0] - (bbox_dimension_delta/2) - (bbox_padding_value/2)
    bbox_y_min_adjusted = detected_bbox[1] - (bbox_padding_value/2)
    bbox_x_max_adjusted = detected_bbox[2] + (bbox_dimension_delta/2) + (bbox_padding_value/2)
    bbox_y_max_adjusted = detected_bbox[3] + (bbox_padding_value/2)
# If x > y, expand the y-value of the bbox and add 50 pixels of padding
elif bbox_dimension_delta < 0:
    bbox_x_min_adjusted = detected_bbox[0] - (bbox_padding_value/2)
    bbox_y_min_adjusted = detected_bbox[1] - (bbox_dimension_delta/2) - (bbox_padding_value/2)
    bbox_x_max_adjusted = detected_bbox[2] + (bbox_padding_value/2)
    bbox_y_max_adjusted = detected_bbox[3] + (bbox_dimension_delta/2) + (bbox_padding_value/2)
# If x = y, add 50 pixels of padding
elif bbox_dimension_delta == 0:
    bbox_x_min_adjusted = detected_bbox[0] - (bbox_padding_value/2)
    bbox_y_min_adjusted = detected_bbox[1] - (bbox_padding_value/2)
    bbox_x_max_adjusted = detected_bbox[2] + (bbox_padding_value/2)
    bbox_y_max_adjusted = detected_bbox[3] + (bbox_padding_value/2)
else:
    raise Exception("Error! Bbox delta value is invalid")

bbox_delta_x_adjusted = bbox_x_max_adjusted - bbox_x_min_adjusted
bbox_delta_y_adjusted = bbox_y_max_adjusted - bbox_y_min_adjusted
print("bbox_delta_x_adjusted: ", bbox_delta_x_adjusted)
print("bbox_delta_y_adjusted: ", bbox_delta_y_adjusted)
if bbox_delta_x_adjusted <= 0 or bbox_delta_y_adjusted <= 0:
    raise Exception("Error! Bbox adjusted co-ordinates are invalid or in the incorrect order")
    
# Checks that adjusted bbox is square
if int(bbox_y_max_adjusted) - int(bbox_y_min_adjusted) != int(bbox_x_max_adjusted) - int(bbox_x_min_adjusted):
   print("Rounded bbox region is not square")

bbox_y_slice_value = int(bbox_y_max_adjusted) - int(bbox_y_min_adjusted)
bbox_x_slice_value = int(bbox_x_max_adjusted) - int(bbox_x_min_adjusted)
print("bbox_y_slice_value:", bbox_y_slice_value)
print("bbox_x_slice_value:", bbox_x_slice_value)
if bbox_y_slice_value > bbox_x_slice_value:
    print("bbox_y_slice_value is larger")
    bbox_x_max_adjusted += (bbox_y_slice_value - bbox_x_slice_value)
elif bbox_x_slice_value > bbox_y_slice_value:
    print("bbox_x_slice_value is larger")
    bbox_y_max_adjusted += (bbox_x_slice_value - bbox_y_slice_value)
elif bbox_y_slice_value == bbox_x_slice_value:
    print("bbox_y_slice_value is the same as bbox_x_slice_value")
else:
    raise Exception("Error! Invalid bbox slice values!")

"""# Using a slice() method here... But I could just calculate the deltas once converted into integers and then make them the same instead
bbox_y_slice = slice(int(bbox_y_min_adjusted), int(bbox_y_max_adjusted))
bbox_x_slice = slice(int(bbox_x_min_adjusted), int(bbox_x_max_adjusted))
print("bbox_y_slice =", bbox_y_slice)
print("bbox_x_slice =", bbox_x_slice)
bbox_bigger_slice = max(bbox_y_slice, bbox_x_slice)
print("bbox_bigger_slice =", bbox_bigger_slice)
#bbox_y_slice = bbox_bigger_slice
#bbox_x_slice = bbox_bigger_slice
print("bbox_y_slice =", bbox_y_slice)
print("bbox_x_slice =", bbox_x_slice)

#Actually I think the slicing method is invalid... Surely the slices need to include the minimum and maximum values for x and y, for the cropping to occur... By providing the same slice to the cropping code, surely this is the same as giving the same bbox co-ordinate values for both x and y, when they should be different? Do the slices indicate the co-ordinates? Surely they must do...
#IT IS CLEAR HERE THAT THE SLICING METHOD IS TO BE AVOIDED... I SHOULD INSTEAD JUST MANIPULATE THE ADJUSTED CO-ORD VALUES SO THAT THEY FORM A SQUARE, BY PERFORMING ANOTHER DIFFERENCE CHECK... I WILL HAVE TO ROUND UP USING CEIL() BEFOREHAND SO TH PADDING IS CONSISTENT? FOR BOTH THE MIN AND MAX VALUES?

cropped = rotated[bbox_y_slice, bbox_x_slice]"""

cropped = rotated[int(bbox_y_min_adjusted):int(bbox_y_max_adjusted), int(bbox_x_min_adjusted):int(bbox_x_max_adjusted)]
# 383:742, 429:789, -> 359, 360
# x slice is the bigger one... due to the value of x_min_adjusted being rounded up to 383
# cropped.shape below is giving me the height and width values the wrong way around... There is a mixup somwhere?
print("cropping done!")

"""Scale"""

# Recalculate width and height of the image
h, w = cropped.shape[:2]
print("cropped: h = ", h, "w = ", w)
if (w != h):
    raise Exception("Error! Crop region is not square!")
# Note that by default python uses double precision floats (i.e. 64 bit representation)
# Numpy specific floats will still be limited to 64 bits, due to the C compiler... check logbook for more information
scaledown_ratio = 256 / h
#print("scaldown_ratio =", scaledown_ratio)
#print(np.finfo(np.longdouble))
# Scale frame up or down to a 256x256 representation
resized = cv2.resize(cropped, None, fx=scaledown_ratio,
                        fy=scaledown_ratio, interpolation=cv2.INTER_AREA)

#preds_resized = fa.get_landmarks(resized)[-1]
preds_resized = fa.get_landmarks_from_image(resized)[-1]

print("scaling done!")


"""Plotting"""

fig = plt.figure(figsize=plt.figaspect(.5))
ax = fig.add_subplot(1, 2, 1)
ax.imshow(resized)

for pred_type in pred_types.values():
    ax.plot(preds_resized[pred_type.slice, 0],
            preds_resized[pred_type.slice, 1],
            color=pred_type.color, **plot_style)

ax.axis('off')
plt.show()

print("plotting done!")