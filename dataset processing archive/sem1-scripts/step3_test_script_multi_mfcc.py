from python_speech_features import mfcc
#from python_speech_features import logfbank
import scipy.io.wavfile as wav
import numpy as np
import os

#frame_rate = 24.9743
frame_rate = 29.95904248021286
clip_num = 2

(rate, sig) = wav.read("vids/dataset/audio" + str(clip_num) + ".wav")

frame_time = 1 / frame_rate
print("frame_time = " + str(frame_time) + "s")

# Returns: A numpy array of size (NUMFRAMES by numcep) containing features. Each row holds 1 feature vector.
# Parameters: numcep – the number of cepstrum to return, default 13
# For a very basic understanding, cepstrum is the information of rate of change in spectral bands

# NEED 146 MFCC rows!!!
# 30 fps gives us 177 rows
# 25 fps gives us 148 rows for video1, that's 146 rows if we discard the top and bottom row?


# mfcc(signal, samplerate=16000, winlen=0.025, winstep=0.01, numcep=13, nfilt=26, nfft=512, lowfreq=0, highfreq=None, preemph=0.97, ceplifter=22, appendEnergy=True, winfunc=<function <lambda>>)
mfcc_feat = mfcc(sig, rate, 0.025, frame_time, 13, 26, 1200)

"""#fbank(signal, samplerate=16000, winlen=0.025, winstep=0.01, nfilt=26, nfft=512, lowfreq=0, highfreq=None, preemph=0.97, winfunc=<function <lambda>>)
fbank_feat = logfbank(sig, rate, 0.025, frame_time, 26, 1200)
print(fbank_feat[1:3, :])"""

new_audio_path = 'data/clip' + str(clip_num) + '/audio/'
os.makedirs(new_audio_path, exist_ok=True)

#numpy.save(file, arr, allow_pickle=True, fix_imports=True)
# Save an array to a binary file in NumPy .npy format.
# Notes: Any data saved to the file is appended to the end of the file.

#np.save("data/clip" + str(clip_num) + "/audio/" + str(mfcc_array_row).zfill(6), mfcc_feat)
np.savetxt("data/clip" + str(clip_num) + "/audio/" +
           str(0).zfill(6) + "full.txt", mfcc_feat)
print("full MFCC saved!")

np.savetxt("data/clip" + str(clip_num) + "/audio/" +
           str(0).zfill(6) + "row0.txt", mfcc_feat[0, :])
print("row0 saved!")

mfcc_array_row = 0
for row in mfcc_feat:
    print(row)
    #np.savetxt("data/clip" + str(clip_num) + "/audio/" + str(mfcc_array_row).zfill(6) + ".txt", mfcc_feat[mfcc_array_row, :])
    np.save("data/clip" + str(clip_num) + "/audio/" +
            str(mfcc_array_row).zfill(6), mfcc_feat[mfcc_array_row, :])
    mfcc_array_row += 1
print("all rows saved!")

# maybe test using np.savetxt() to see what the mfcc array output actually is first?
# then figure out how to separate each row, i.e. how to access each row individually, is it mfcc_feat[1:3, :] for example?
