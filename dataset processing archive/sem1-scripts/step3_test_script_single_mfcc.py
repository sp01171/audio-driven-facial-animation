from python_speech_features import mfcc
from python_speech_features import logfbank
import scipy.io.wavfile as wav
import numpy as np

frame_rate = 30
clip_num = 1

(rate, sig) = wav.read("vids/dataset/audio" + str(clip_num) + ".wav")

frame_time = 1 / frame_rate
print("frame_time = " + str(frame_time) + "s")

#Returns: A numpy array of size (NUMFRAMES by numcep) containing features. Each row holds 1 feature vector.
#Parameters: numcep – the number of cepstrum to return, default 13
#For a very basic understanding, cepstrum is the information of rate of change in spectral bands

#mfcc(signal, samplerate=16000, winlen=0.025, winstep=0.01, numcep=13, nfilt=26, nfft=512, lowfreq=0, highfreq=None, preemph=0.97, ceplifter=22, appendEnergy=True, winfunc=<function <lambda>>)
mfcc_feat = mfcc(sig, rate, 0.025, frame_time, 13, 26, 1200)

"""#fbank(signal, samplerate=16000, winlen=0.025, winstep=0.01, nfilt=26, nfft=512, lowfreq=0, highfreq=None, preemph=0.97, winfunc=<function <lambda>>)
fbank_feat = logfbank(sig, rate, 0.025, frame_time, 26, 1200)
print(fbank_feat[1:3, :])"""

#numpy.save(file, arr, allow_pickle=True, fix_imports=True)
#Save an array to a binary file in NumPy .npy format.
#Notes: Any data saved to the file is appended to the end of the file.

np.save("data/clip" + str(clip_num) + "/mfcc", mfcc_feat)
print("MFCC saved!")