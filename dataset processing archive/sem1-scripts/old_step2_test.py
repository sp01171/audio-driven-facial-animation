# Necessary imports
import cv2
import numpy as np
import os
import shutil

# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture('D:vids/dataset/video1.mp4')

# Check if camera opened successfully
if (cap.isOpened() == False):
    print("Error opening video stream or file")

# NEED TO ADD ERROR CHECKING / EXCEPTION HANDLING HERE!!!
new_path = 'data/clip1/images/'
# Deletes any previous data folder and old frames
shutil.rmtree('data/')
# Creates data folder and other subdirectories, throws error if it already exists
os.makedirs(new_path, exist_ok=False)

# Read until video is completed
i = 0
while (cap.isOpened()):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret == True:
        cv2.imwrite((new_path + 'image' + str(i).zfill(4) + '.jpg'), frame)
        i += 1

        """# Display the resulting frame
        cv2.imshow('Frame', frame)

        # Press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break"""

    # Break the loop
    else:
        break

# When everything done, release the video capture object
cap.release()

# Creating face_cascade and eye_cascade objects
face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
eye_cascade = cv2.CascadeClassifier("haarcascade_eye.xml")

# Loading the image
#img = cv2.imread('data/clip1/images/image0000.jpg')
img = cv2.imread('angledshot.jpg')

dimensions = img.shape
print(dimensions)
y = dimensions[0]
x = dimensions[1]
print(y, x)
# This assumes that the image never taller than it is wide
if (y > 1920):
    pixel_y_scale = 1080 / y
    # scaling to maintain vertical FOV
    img = cv2.resize(img, None, fx = pixel_y_scale, fy = pixel_y_scale, interpolation = cv2.INTER_AREA)

winname = 'image0000.jpg'
cv2.namedWindow(winname)        # Create a named window
cv2.moveWindow(winname, 40, 30)  # Move it to (40,30)
#cv2.imshow('emily', img)

# Converting the image into grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# Creating variable faces
faces = face_cascade.detectMultiScale(gray, 1.1, 4)
# Defining and drawing the rectangle around the face
for (x, y,  w,  h) in faces:
    # print("passed")
    # Crops around face
    print("faces: x =", x, "y =", y, "w =", w, "h =", h)
    img = img[y:(y+h), x:(x+w)]
    pass
    #cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 3)
#cv2.imshow('emily.jpg', img)

# Creating two regions of interest
print("ROI: x =", x, "y =", y, "w =", w, "h =", h)
roi_gray = gray[y:(y+h), x:(x+w)]
roi_color = img[y:(y+h), x:(x+w)]

# Creating variable eyes
eyes = eye_cascade.detectMultiScale(roi_gray, 1.1, 4)
index = 0
# Creating for loop in order to divide one eye from another
for (ex, ey,  ew,  eh) in eyes:
    if index == 0:
        eye_1 = (ex, ey, ew, eh)
    elif index == 1:
        eye_2 = (ex, ey, ew, eh)
# Drawing rectangles around the eyes
    #cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 0, 255), 3)
    index = index + 1
#cv2.imshow(winname, img)

# Assumes that left eye is smaller!!! - need to relook at this later
if eye_1[0] < eye_2[0]:
    left_eye = eye_1
    right_eye = eye_2
else:
    left_eye = eye_2
    right_eye = eye_1

# Calculating coordinates of a central points of the rectangles
left_eye_center = (int(left_eye[0] + (left_eye[2] / 2)),
                   int(left_eye[1] + (left_eye[3] / 2)))
left_eye_x = left_eye_center[0]
left_eye_y = left_eye_center[1]

right_eye_center = (
    int(right_eye[0] + (right_eye[2]/2)), int(right_eye[1] + (right_eye[3]/2)))
right_eye_x = right_eye_center[0]
right_eye_y = right_eye_center[1]

"""cv2.circle(roi_color, left_eye_center, 5, (255, 0, 0), -1)
cv2.circle(roi_color, right_eye_center, 5, (255, 0, 0), -1)
cv2.line(roi_color, right_eye_center, left_eye_center, (0, 200, 200), 3)"""

if left_eye_y > right_eye_y:
    A = (right_eye_x, left_eye_y)
    # Integer -1 indicates that the image will rotate in the clockwise direction
    direction = -1
else:
    A = (left_eye_x, right_eye_y)
   # Integer 1 indicates that image will rotate in the counter clockwise
   # direction
    direction = 1

"""cv2.circle(roi_color, A, 5, (255, 0, 0), -1)

cv2.line(roi_color, right_eye_center, left_eye_center, (0, 200, 200), 3)
cv2.line(roi_color, left_eye_center, A, (0, 200, 200), 3)
cv2.line(roi_color, right_eye_center, A, (0, 200, 200), 3)"""
#cv2.imshow(winname, img)

delta_x = right_eye_x - left_eye_x
delta_y = right_eye_y - left_eye_y
angle = np.arctan(delta_y/delta_x)
angle = (angle * 180) / np.pi

# Width and height of the image
h, w = img.shape[:2]
# Calculating a center point of the image
# Integer division "//"" ensures that we receive whole numbers
center = (w // 2, h // 2)
# Defining a matrix M and calling
# cv2.getRotationMatrix2D method
M = cv2.getRotationMatrix2D(center, (angle), 1.0)
# Applying the rotation to our image using the
# cv2.warpAffine method
rotated = cv2.warpAffine(img, M, (w, h))
#cv2.imshow(winname, rotated)

"""for (x, y,  w,  h) in faces:
    crop_img = rotated[y:(y+h), x:(x+w)]
    #crop_img = rotated[y-100:(y+h)+100, x-100:(x+w)+100]
cv2.imshow(winname, crop_img)"""
#cv2.imshow(winname, rotated)

# insert code here to scale down to 256x256
# current code is 581 x 581 (but specifically for video1, image0000), i.e. image0000 is cropping to 581 x 581, it needs to be dynamic

if (w != h):
    raise Exception("Error! Crop region is not square!")

# note that by default python uses double precision floats (i.e. 64 bit representation)
# numpy specific floats will still be limited to 64 bits, due to the C compiler... check logbook for more information
scaledown_ratio = 256 / h
print("scaldown_ratio =", scaledown_ratio)
#print(np.finfo(np.longdouble))
#resized = cv2.resize(img, (256, 256), interpolation = cv2.INTER_AREA)
resized = cv2.resize(rotated, None, fx = scaledown_ratio, fy = scaledown_ratio, interpolation = cv2.INTER_AREA)
cv2.imshow(winname, resized)

cv2.waitKey(5000)
"""while (1):
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break"""
cv2.destroyAllWindows()

"""Notes:
Everything above needs to be implemented into a function.
The faces need to be tightly cropped (I'm assuming I will have to use the rectangle?)...
    Or maybe I can scale up from the rectangle to ensure that my hair is within the crop?
The largest image needs to be scaled to 256x256, and then I'm assuming every other smaller image(?) will need to be scaled against that... Not sure how this works...
According to Marco, the function will work in the following order? Crop, align, resize...
"""
