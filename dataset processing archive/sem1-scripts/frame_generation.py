import cv2
#import numpy as np
import os
import shutil

# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture('D:vids/dataset/video1.mp4')

# Check if camera opened successfully
if (cap.isOpened() == False):
    print("Error opening video stream or file")

# NEED TO ADD ERROR CHECKING / EXCEPTION HANDLING HERE!!!
new_path = 'data/clip1/images/'
# Deletes any previous data folder and old frames
shutil.rmtree('data/')
# Creates data folder and other subdirectories, throws error if it already exists
os.makedirs(new_path, exist_ok=False)

# Read until video is completed
i = 0
while (cap.isOpened()):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret == True:
        cv2.imwrite((new_path + 'image' + str(i).zfill(4) + '.jpg'), frame)
        i += 1

        """# Display the resulting frame
        cv2.imshow('Frame', frame)

        # Press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break"""

    # Break the loop
    else:
        break

# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()
