import face_alignment
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
#import collections
import numpy as np
import cv2
import time
import os
import shutil
import pickle
from python_speech_features import mfcc
import scipy.io.wavfile as wav

#DATASET_SIZE = 1
DATASET_SIZE = 20

# Optionally set detector and some additional detector parameters
face_detector = 'sfd'
face_detector_kwargs = {
    "filter_threshold" : 0.8
}

# Run the 2D face alignment on a test image, without CUDA.
fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, device='cuda', flip_input=True,
                                  face_detector=face_detector, face_detector_kwargs=face_detector_kwargs, verbose = True)


def extract_frames_and_mfccs(vid_num):
    """Extract frames from the dataset video, and MFCCs features from dataset audio

    Keyword arguments:
    vid_num -- the number of the current video file
    """


    def audio_processing(clip_num, frame_rate, frame_count):
        """Process audio clips via MFCC descriptor.

        Keyword arguments:
        clip_num -- the number of the current video file
        frame_rate -- the frame rate of the current video file
        frame_count -- the total number of frames in the current video file
        """
        print("frame_rate =", frame_rate)
        print("frame_count =", frame_count)
        print("estimated video length = " + str(frame_count/frame_rate) + "s")

        (rate, sig) = wav.read("vids/dataset/audio" + str(clip_num) + ".wav")

        #frame_rate = 30
        #frame_rate = 29.95904248021286
        frame_time = 1 / frame_rate
        print("frame_time = " + str(frame_time) + "s")

        # Returns: A numpy array of size (NUMFRAMES by numcep) containing features. Each row holds 1 feature vector.
        # Parameters: numcep – the number of cepstrum to return, default 13
        # For a very basic understanding, cepstrum is the information of rate of change in spectral bands
        # mfcc(signal, samplerate=16000, winlen=0.025, winstep=0.01, numcep=13, nfilt=26, nfft=512, lowfreq=0, highfreq=None, preemph=0.97, ceplifter=22, appendEnergy=True, winfunc=<function <lambda>>)
        #mfcc_feat = mfcc(sig, rate, 0.025, frame_time, 13, 26, 1200)
        mfcc_feat = mfcc(signal=sig, samplerate=rate, winlen=0.025, winstep=frame_time, numcep=13, nfilt=26, nfft=1200, lowfreq=0, highfreq=None, preemph=0.97, ceplifter=22, appendEnergy=True)
        """Note that the code api says that highfreq is samplerate/2 by default, but the actual source code shows by default it's set to None? - NEED TO SEE IF THIS CHANGES THE RESULT?"""
        """Change appendEnergy=True to appendEnergy=False? I think we're losing the first coefficient here? Will need to do a before and after of values to check..."""

        #numpy.save(file, arr, allow_pickle=True, fix_imports=True)
        # Save an array to a binary file in NumPy .npy format.
        # Note: Any data saved to the file is appended to the end of the file.

        return mfcc_feat


    def rescale_image(img, vertical_res):
        """Downscale/Upscale"""

        dimensions = img.shape
        #print(dimensions)
        y = dimensions[0]
        #x = dimensions[1]
        #print(y, x)
        # Assumes the image is never taller than it is wide
        # Always scale image to 1080p vertical resolution, regardless of the horizontal resolution / aspect ratio
        if (y != vertical_res):
            pixel_y_scale = vertical_res / y
            # Downscale frame to 1920x1080 while maintaining vertical FOV / aspect ratio
            img = cv2.resize(img, None, fx=pixel_y_scale,
                                fy=pixel_y_scale, interpolation=cv2.INTER_AREA)
        #print(img.shape)

        return img

    
    # Create a VideoCapture object and read from input file
    # If the input is the camera, pass 0 instead of the video file name
    #print(vid_num)
    print('\nvids/dataset/video' + str(vid_num) + '.mp4')
    cap = cv2.VideoCapture('vids/dataset/video' + str(vid_num) + '.mp4')
    
    #cap.set(cv2.CAP_PROP_FRAME_HEIGHT)
    vid_fps = cap.get(cv2.CAP_PROP_FPS)
    vid_total_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    print("result of cap.get(FPS) is: ", vid_fps)
    print("result of cap.get(FRAME_COUNT) is: ", vid_total_frames)

    # Check if camera opened successfully
    if (cap.isOpened() == False):
        raise Exception("Error opening video stream or file")

    # Creates a list to hold the frames from the video
    #frame_list = []

    # Read until video is completed
    i = 0
    while (cap.isOpened()):
        new_frame_path = ('data/clip' + str(vid_num) + '/images/image' + str(i).zfill(4) + '.jpg')
        # Capture frame-by-frame
        ret, frame = cap.read()
        if ret == True:
            print("vid", vid_num, "frame", i)
            rescaled_frame = rescale_image(frame, 1080)
            cv2.imwrite(new_frame_path, frame)
            #frame_list.append(rescaled_frame)
            i += 1
        # Break the loop
        else:
            break

    # When everything done, release the video capture object
    cap.release()

    #return frame_list

    # Calculate MFCC of the audio recording
    mfcc_array = audio_processing(vid_num, vid_fps, vid_total_frames)
    
    # Separate 2D MFCC array into frame-specific values (each line or row of the 2D MFCC array) and store as individual files
    row_num = 0
    for row in mfcc_array:
        #print(row)
        print("mfcc_array[row_num, :] =", mfcc_array[row_num, :])
        np.savetxt("data/clip" + str(vid_num) + "/audio/" + str(row_num).zfill(6) + ".txt", mfcc_array[row_num, :])
        np.save("data/clip" + str(vid_num) + "/audio/" +
                str(row_num).zfill(6), mfcc_array[row_num, :])
        row_num += 1
    print("all mfcc rows saved!")

    return


def rotate_frames(vid_num):
    """Calculate preds, calculate nose-chin angles using preds data, load frames, rotate frames, and save frames as jpgs

    Keyword arguments:
    vid_num -- the number of the current video file
    """

    # Returns a dictionary with key "image path" and value "preds array"
    predictions = fa.get_landmarks_from_directory(("data/clip" + str(vid_num) + "/images/"), extensions='.jpg', recursive=False, show_progress_bar=True,
                                        return_bboxes=False, return_landmark_score=False)
    #print(predictions)
    #print(type(predictions))
    
    i = 0
    for array in predictions.values():
        new_frame_path = ('data/clip' + str(vid_num) + '/images/image' + str(i).zfill(4) + '.jpg')
        print(new_frame_path)

        """calculate nose-chin angles using preds data"""
        #print("Array =", array)
        #print("Array[-1] =", array[-1])
        #print("nose bridge tuple is", array[27])
        #print("nose tuple is", array[30])
        #print("chin tuple is", array[8])
        # Removes extra unnecessary data, like the dtype of the array data (float32)
        array = array[-1]
        delta_x = array[27][0] - array[8][0]
        #print("delta_x =", delta_x)
        # Note that the calculation is inverted due to the y-axis being inverted, co-ords are taken from the top left corner of the image!!!
        delta_y = array[8][1] - array[27][1]
        #print("delta_y =", delta_y)
        angle = np.arctan(delta_x/delta_y)
        angle = ((angle * 180) / np.pi)
        print("Rotation angle in degrees is " + str(angle))

        """load frames"""
        # Will default to cv2.IMREAD_COLOR() instead of cv2.IMREAD_UNCHANGED(), expecting the image to be in BGR instead of RGB, cv2.IMREAD_ANY_COLOR can read in RGB!
        img = cv2.imread(new_frame_path)

        """rotate frames"""
        h, w = img.shape[:2]
        #print("img: h = ", h, "w = ", w)
        # Defining a matrix M and calling
        # cv2.getRotationMatrix2D method
        M = cv2.getRotationMatrix2D(array[27], (angle), 1.0)
        # Applying the rotation to our image using the
        # cv2.warpAffine method
        rotated = cv2.warpAffine(img, M, (w, h))
        
        """save frames as jpgs"""
        cv2.imwrite(new_frame_path, rotated)

        i += 1

    return


def crop_frames(vid_num):
    """Calculate preds (with bounding boxes), load frames, crop frames, resize frames to 256x256, and save as jpgs

    Keyword arguments:
    vid_num -- the number of the current video file
    """


    def calculate_crop_region(detected_bbox):
        
        bbox_x_min = int(detected_bbox[0])
        bbox_y_min = int(detected_bbox[1])
        bbox_x_max = int(detected_bbox[2])
        bbox_y_max = int(detected_bbox[3])

        bbox_delta_x = bbox_x_max - bbox_x_min
        bbox_delta_y = bbox_y_max - bbox_y_min
        #print("bbox_delta_x: ", bbox_delta_x)
        #print("bbox_delta_y: ", bbox_delta_y)
        if bbox_delta_x <= 0 or bbox_delta_y <= 0:
            raise Exception("Error! Bbox co-ordinates are invalid or in the incorrect order")

        # Check which bbox dimension is largest
        # If delta is positive then y > x, if delta is negative then x > y
        # Bbox height is usually greater than width, due to face shapes being taller than than they are wide
        bbox_dimension_delta = bbox_delta_y - bbox_delta_x
        #print("bbox_dimension_delta =", bbox_dimension_delta)
        #bbox_padding_value = 50
        bbox_padding_value = 0
        # Even if expanding a dimension causes .5 of a pixel to be added to either side (min and max values), then upon the second int() conversion they will both be rounded up, so the difference between them will remain the same!
        # If y > x, expand the x-value of the bbox and add 50 pixels of padding
        if bbox_dimension_delta > 0:
            bbox_x_min_adjusted = detected_bbox[0] - (bbox_dimension_delta/2) - (bbox_padding_value/2)
            bbox_y_min_adjusted = detected_bbox[1] - (bbox_padding_value/2)
            bbox_x_max_adjusted = detected_bbox[2] + (bbox_dimension_delta/2) + (bbox_padding_value/2)
            bbox_y_max_adjusted = detected_bbox[3] + (bbox_padding_value/2)
        # If x > y, expand the y-value of the bbox and add 50 pixels of padding
        elif bbox_dimension_delta < 0:
            bbox_x_min_adjusted = detected_bbox[0] - (bbox_padding_value/2)
            bbox_y_min_adjusted = detected_bbox[1] - (bbox_dimension_delta/2) - (bbox_padding_value/2)
            bbox_x_max_adjusted = detected_bbox[2] + (bbox_padding_value/2)
            bbox_y_max_adjusted = detected_bbox[3] + (bbox_dimension_delta/2) + (bbox_padding_value/2)
        # If x = y, add 50 pixels of padding
        elif bbox_dimension_delta == 0:
            bbox_x_min_adjusted = detected_bbox[0] - (bbox_padding_value/2)
            bbox_y_min_adjusted = detected_bbox[1] - (bbox_padding_value/2)
            bbox_x_max_adjusted = detected_bbox[2] + (bbox_padding_value/2)
            bbox_y_max_adjusted = detected_bbox[3] + (bbox_padding_value/2)
        else:
            raise Exception("Error! Bbox delta value is invalid")

        bbox_delta_x_adjusted = bbox_x_max_adjusted - bbox_x_min_adjusted
        bbox_delta_y_adjusted = bbox_y_max_adjusted - bbox_y_min_adjusted
        #print("bbox_delta_x_adjusted: ", bbox_delta_x_adjusted)
        #print("bbox_delta_y_adjusted: ", bbox_delta_y_adjusted)
        if bbox_delta_x_adjusted <= 0 or bbox_delta_y_adjusted <= 0:
            raise Exception("Error! Bbox adjusted co-ordinates are invalid or in the incorrect order")
            
        # Checks that adjusted bbox is square
        if int(bbox_y_max_adjusted) - int(bbox_y_min_adjusted) != int(bbox_x_max_adjusted) - int(bbox_x_min_adjusted):
            #print("Rounded bbox region is not square")
            pass

        bbox_y_slice_value = int(bbox_y_max_adjusted) - int(bbox_y_min_adjusted)
        bbox_x_slice_value = int(bbox_x_max_adjusted) - int(bbox_x_min_adjusted)
        #print("bbox_y_slice_value:", bbox_y_slice_value)
        #print("bbox_x_slice_value:", bbox_x_slice_value)
        if bbox_y_slice_value > bbox_x_slice_value:
            #print("bbox_y_slice_value is larger")
            bbox_x_max_adjusted += (bbox_y_slice_value - bbox_x_slice_value)
        elif bbox_x_slice_value > bbox_y_slice_value:
            #print("bbox_x_slice_value is larger")
            bbox_y_max_adjusted += (bbox_x_slice_value - bbox_y_slice_value)
        elif bbox_y_slice_value == bbox_x_slice_value:
            #print("bbox_y_slice_value is the same as bbox_x_slice_value")
            pass
        else:
            raise Exception("Error! Invalid bbox slice values!")

        return bbox_y_min_adjusted, bbox_y_max_adjusted, bbox_x_min_adjusted, bbox_x_max_adjusted
    
    # Returns a dictionary with key "image path" and value "preds array", with bounding boxes
    predictions = fa.get_landmarks_from_directory(("data/clip" + str(vid_num) + "/images/"), extensions='.jpg', recursive=False, show_progress_bar=True,
                                        return_bboxes=True, return_landmark_score=False)
    #print("predictions =", predictions)

    i = 0
    for array in predictions.values():
        #print("array =", array)
        #print("array[-1] =", array[-1])
        #print("array[-1][0] =", array[-1][0])
        #print("detected_bbox =", array[-1][0])
        new_frame_path = ('data/clip' + str(vid_num) + '/images/image' + str(i).zfill(4) + '.jpg')

        """load frames"""
        # Will default to cv2.IMREAD_COLOR() instead of cv2.IMREAD_UNCHANGED(), expecting the image to be in BGR instead of RGB, cv2.IMREAD_ANY_COLOR can read in RGB!
        img = cv2.imread(new_frame_path)

        """crop frames"""
        bbox_y_min_adjusted, bbox_y_max_adjusted, bbox_x_min_adjusted, bbox_x_max_adjusted = calculate_crop_region(detected_bbox=array[-1][0])
        cropped = img[int(bbox_y_min_adjusted):int(bbox_y_max_adjusted), int(bbox_x_min_adjusted):int(bbox_x_max_adjusted)]

        """resize frames to 256x256"""
        # Recalculate width and height of the image
        h, w = cropped.shape[:2]
        #print("cropped: h = ", h, "w = ", w)
        if (w != h):
            raise Exception("Error! Crop region is not square!")
        # Note that by default python uses double precision floats (i.e. 64 bit representation)
        # Numpy specific floats will still be limited to 64 bits, due to the C compiler... check logbook for more information
        scaledown_ratio = 256 / h
        #print("scaldown_ratio =", scaledown_ratio)
        #print(np.finfo(np.longdouble))
        # Scale frame up or down to a 256x256 representation
        resized = cv2.resize(cropped, None, fx=scaledown_ratio,
                                fy=scaledown_ratio, interpolation=cv2.INTER_AREA)
        
        """save frames as jpgs"""        
        cv2.imwrite(new_frame_path, resized)

        i += 1

    return


def export_landmarks(vid_num):
    """Calculate preds (with landmark confidence scores), and save as pickle files

    Keyword arguments:
    vid_num -- the number of the current video file
    """
    
    """calculate preds (with landmark confidence scores)"""
    # Returns a dictionary with key "image path" and value "preds array", with landmark confidence scores
    predictions = fa.get_landmarks_from_directory(("data/clip" + str(vid_num) + "/images/"), extensions='.jpg', recursive=False, show_progress_bar=True,
                                        return_bboxes=False, return_landmark_score=True)
    print("predictions =", predictions)
    
    """save landmark data and confidence scores for each frame as pickle files"""
    i = 0
    for array in predictions.values():
        #print(array)
        new_landmark_path = ('data/clip' + str(vid_num) + '/landmarks/' + str(i).zfill(4) + '.p')
        #np.savetxt(('data/preds' + str(i).zfill(4) + '.txt'), array)
        # With-statement causes file to close as soon as the indented code below is executed
        with open(new_landmark_path, "wb") as f:
            pickle.dump(array, f)
        i += 1

    return


def data_processing(vid_num):
    
    """Executes video processing and audio processing code from functions above
    """

    frame_folder_path = 'data/clip' + str(vid_num) + '/images/'
    audio_folder_path = 'data/clip' + str(vid_num) + '/audio/'
    landmark_folder_path = 'data/clip' + str(vid_num) + '/landmarks/'
    # Creates data folder and other subdirectories, throws error if it already exists
    os.makedirs(frame_folder_path, exist_ok=False)
    # Changed the two lines below to be False instead of True, not sure if this will throw an error?
    os.makedirs(audio_folder_path, exist_ok=False)
    os.makedirs(landmark_folder_path, exist_ok=False)

    # Creates a 2D array to hold the frames from each video, in separate lists
    #frame_array = []
    #frame_array.append(extract_frames(vid_num))
    #print(frame_array)

    print("Extracting frames and MFCCs:")
    extract_frames_and_mfccs(vid_num)

    print("Rotating frames:")
    rotate_frames(vid_num)
    
    """Calculate preds (with bounding boxes), load frames, crop frames, resize frames to 256x256, and save as jpgs"""
    print("Cropping and resizing frames:")
    crop_frames(vid_num)

    """Calculate preds (with landmark confidence scores), and save as pickle files"""
    print("Exporting landmark data:")
    export_landmarks(vid_num)

    print("video" + str(vid_num) + " processed!\n")

    return


# Deletes any previous data folder and old frames
print("about to delete data folder")
try:
    shutil.rmtree('data/')
    print("data folder deleted")
except FileNotFoundError:
    print("data folder did not exist")
except PermissionError:
    print("Error! Cannot delete data folder, an internal file is currently open... Close all open file(s) and try again!")
    exit(0)

for i in range(DATASET_SIZE):
    data_processing(vid_num = i+1)

print("complete!")
