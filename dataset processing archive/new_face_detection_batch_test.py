import face_alignment

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import collections
import numpy as np
import cv2
import time
import os
import shutil
import pickle

def data_processing(vid_num):
    """Process the dataset of video and audio files for neural network training.

    Keyword arguments:
    vid_num -- the number of the current video file
    face_cascade -- pretrained haar cascade classifier model for detecting faces
    eye_cascade -- pretrained haar cascade classifier model for detecting eyes
    """
    # Create a VideoCapture object and read from input file
    # If the input is the camera, pass 0 instead of the video file name
    #print(vid_num)
    print('\nvids/dataset/video' + str(vid_num) + '.mp4')
    cap = cv2.VideoCapture('vids/dataset/video' + str(vid_num) + '.mp4')
    
    #cap.set(cv2.CAP_PROP_FRAME_HEIGHT)
    vid_fps = cap.get(cv2.CAP_PROP_FPS)
    vid_total_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    print("result of cap.get(FPS) is: ", vid_fps)
    print("result of cap.get(FRAME_COUNT) is: ", vid_total_frames)

    # Check if camera opened successfully
    if (cap.isOpened() == False):
        raise Exception("Error opening video stream or file")

    new_frame_path = 'data/clip' + str(vid_num) + '/images/'

    # Creates data folder and other subdirectories, throws error if it already exists
    os.makedirs(new_frame_path, exist_ok=False)

    # Read until video is completed
    i = 0
    while (cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()
        if ret == True:
            print("vid", vid_num, "frame", i)
            cv2.imwrite(
                (new_frame_path + 'image' + str(i).zfill(4) + '.jpg'), frame)
            i += 1
        # Break the loop
        else:
            break

    # When everything done, release the video capture object
    cap.release()

    return

""" CREATE IMAGES """

"""# Deletes any previous data folder and old frames
print("about to delete data folder")
try:
    shutil.rmtree('data/')
    print("data folder deleted")
except FileNotFoundError:
    print("data folder did not exist")
except PermissionError:
    print("Error! Cannot delete data folder, an internal file is currently open... Close all open file(s) and try again!")
    exit(0)

# Executes data processing code from functions above
for i in range(20):
    data_processing(i+1)
print("complete!")"""


""" DO STUFF """

# Optionally set detector and some additional detector parameters
face_detector = 'sfd'
face_detector_kwargs = {
    "filter_threshold" : 0.8
}

# WHAT IS THIS FLIP INPUT ARGUMENT????!!!!!

# Run the 2D face alignment on a test image, without CUDA.
fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, device='cuda', flip_input=True,
                                  face_detector=face_detector, face_detector_kwargs=face_detector_kwargs, verbose = True)

# preds_rotated, landmark_score, detected_bbox = fa.get_landmarks_from_image(rotated, detected_faces=None, return_bboxes=True, return_landmark_score=False)

"""get_landmarks_from_directory(self, path, extensions=['.jpg', '.png'], recursive=True, show_progress_bar=True,
                                     return_bboxes=False, return_landmark_score=False)"""


start_time = time.time()

"""
input_img = cv2.imread('/data/clip1/images/image0000.jpg')
preds_rotated, landmark_score, detected_bbox = fa.get_landmarks_from_image(input_img, detected_faces=None, return_bboxes=True, return_landmark_score=False)
"""

# Returns a dictionary with key "image path" and value "preds array"
predictions = fa.get_landmarks_from_directory("data/clip1/images/", extensions='.jpg', recursive=True, show_progress_bar=True,
                                     return_bboxes=True, return_landmark_score=False)

#print(predictions)

print(type(predictions))

"""Save calculated landmarks for each frame"""

# Add code to create numbered folders (e.g. clip1/_, clip2/_, etc.) for the output preds files below...
i = 0
for array in predictions.values():
    print(array)    
    #Exception has occurred: TypeError
    #Mismatch between array dtype ('object') and format specifier ('%.18e')
    #TypeError: must be real number, not list
    #np.savetxt(('data/preds' + str(i).zfill(4) + '.txt'), array)
    pickle.dump(array, open('data/preds' + str(i).zfill(4) + '.p', "wb"))
    i += 1
    print("Exported preds" + str(i).zfill(4) + '.txt')

print("--- %s seconds ---" % (time.time() - start_time))
